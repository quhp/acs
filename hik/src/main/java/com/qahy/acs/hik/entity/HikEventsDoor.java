package com.qahy.acs.hik.entity;

import com.qahy.acs.hik.common.bean.HikBaseEntity;
import lombok.Data;

/**
 * @version 1.0
 * @Description //TODO 门禁点事件
 * @Author jujian
 * @Date 2021/11/19 13:59
 **/
@Data
public class HikEventsDoor extends HikBaseEntity {

    private String eventId;
    private String eventName;
    private String eventTime;
    private String cardNo;
    private String personId;
    private String orgIndexCode;
    private String picUri;
    private String svrIndexCode;
    private Integer eventType;
    private Integer inAndOutType;
    private String readerDevIndexCode;
    private String readerDevName;
    private String devIndexCode;
    private String devName;
    private String identityCardUri;
    private String receiveTime;
    private String jobNo;
    private String studentId;
//    private Object certNo;

}
