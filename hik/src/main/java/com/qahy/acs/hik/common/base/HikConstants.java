package com.qahy.acs.hik.common.base;

import org.springframework.stereotype.Component;

/**
 * @version 1.0
 * @Description //TODO 常量
 * @Author jujian
 * @Date 2021/11/9 14:39
 **/
@Component
public class HikConstants {

    /**
     * STEP1：设置平台参数，根据实际情况,设置host appkey appsecret 三个参数.
     */
    // artemis网关服务器ip端口
    public static String HOST = "ip:port";
    // 秘钥appkey
    public static String APPKEY = "APPKEY";
    // 秘钥appSecret
    public static String APPSECRET = "APPSECRET";
    /**
     * STEP4：设置参数提交方式
     */
    public static String CONTENTTYPE = "application/json";
    /**
     * STEP2：设置OpenAPI接口的上下文
     */
    public static String ARTEMISPATH = "/artemis";

    //===============================请求地址==============

    //批量添加组织
    public static String ORG_ADDS = "/api/resource/v1/org/batch/add";
    //批量删除组织
    public static String ORG_DELETES = "/api/resource/v1/org/batch/delete";
    //批量添加人员
    public static String PERSON_ADDS = "/api/resource/v1/person/batch/add";
    //批量删除人员
    public static String PERSON_DELETES = "/api/resource/v1/person/batch/delete";
    //获取人员
    public static String PERSON_INFO = "/api/resource/v2/person/advance/personList";
    //添加人脸
    public static String SINGLE_ADD = "/api/resource/v1/face/single/add";
    //删除人脸
    public static String SINGLE_DELETE = "/api/resource/v1/face/single/delete";
    //出入权限配置
    public static String AUTH_CONFIG_ADD = "/api/acps/v1/auth_config/add";
    //出入权限配置
    public static String AUTH_CONFIG_DELETE = "/api/acps/v1/auth_config/delete";
    //根据出入权限配置快捷下载
    public static String CONFIG_URATION_SHORTCUT = "/api/acps/v1/authDownload/configuration/shortcut";
    //查詢門禁設備（控制器）
    public static String DEVEICE_ASC_URL = "/api/resource/v2/acsDevice/search";
    //查詢門禁設備（門禁點）
    public static String DEVEICE_DOOR_URL = "/api/resource/v2/door/search";
    //查詢門禁事件
    public static String EVENTS_DOOR = "/api/acs/v2/door/events";


//    //根据出入权限配置快捷下载
//    public static String EVENTS_DOOR = "/api/acps/v1/authDownload/configuration/shortcut";

}
