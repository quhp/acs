package com.qahy.acs.hik.entity;

import com.qahy.acs.hik.common.bean.HikBaseEntity;
import lombok.Data;

/**
 * @version 1.0
 * @Description //TODO 海康门禁设备
 * @Author jujian
 * @Date 2021/11/18 14:27
 **/
@Data
public class HikDeviceAcs extends HikBaseEntity {

//    private String id;
    private String indexCode;
    private String resourceType;
    private String name;
    private String parentIndexCode;
    private String regionIndexCode;
    private String regionPath;
    private String devTypeCode;
    private String devTypeDesc;
    private String deviceCode;
    private String manufacturer;
    private String treatyType;
    private Integer cardCapacity;
    private Integer fingerCapacity;
    private Integer veinCapacity;
    private Integer faceCapacity;
    private Integer doorCapacity;
    private String deployId;
    private String createTime;
    private String updateTime;
    private String description;
    private String netZoneId;
    private String acsReaderVerifyModeAbility;
    private String regionName;
    private String regionPathName;
    private String ip;
    private String port;
    private String capability;
    private String devSerialNum;
    private String dataVersion;
    private String onLine;
//    private String status;
//    private String acsCreateId;
//    private Date acsCreateTime;
//    private String acsUpdateId;
//    private Date acsUpdateTime;
//    private String deleted;

}
