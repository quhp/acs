package com.qahy.acs.hik.common.bean;

import lombok.Data;

import java.util.Date;

/**
 * @version 1.0
 * @Description //TODO
 * @Author jujian
 * @Date 2021/11/19 14:00
 **/
@Data
public class HikBaseEntity {

    private String id;

    private String status;
    private String acsCreateId;
    private Date acsCreateTime;
    private String acsUpdateId;
    private Date acsUpdateTime;
    private String deleted;
}
