package com.qahy.acs.hik.entity;

/**
 * @version 1.0
 * @Description //TODO 海康设备
 * @Author jujian
 * @Date 2021/11/17 13:27
 **/
public class HikDevice {

    /***
     * 资源的唯一标识，32位数字+字母（小写）字符串；资源为设备
     **/
    private String resourceIndexCode;

    //资源类型
    private String resourceType;
    //通道号，当资源类型为设备时，无特殊说明必填
    private int[] channelNos;

    //开始日期，
    //2018-09-03T17:30:08.000+08:00
    private String startTime;

    //结束日期
    private String endTime;
}
