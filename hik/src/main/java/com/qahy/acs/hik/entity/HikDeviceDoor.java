package com.qahy.acs.hik.entity;

import com.qahy.acs.hik.common.bean.HikBaseEntity;
import lombok.Data;

/**
 * @version 1.0
 * @Description //TODO 海康门禁
 * @Author jujian
 * @Date 2021/11/18 14:28
 **/
@Data
public class HikDeviceDoor extends HikBaseEntity {

//    private String id;
    private String indexCode;
    private String resourceType;
    private String name;
    private String doorNo;
    private String channelNo;
    private String parentIndexCode;
    private String controlOneId;
    private String controlTwoId;
    private String readerInId;
    private String readerOutId;
    private String treatyType;
    private String doorSerial;
    private String regionIndexCode;
    private String regionPath;
    private String createTime;
    private String updateTime;
    private String description;
    private String channelType;
    private String regionName;
    private String regionPathName;
    private String installLocation;
    private String onLine;
//    private String status;
//    private String acsCreateId;
//    private Date acsCreateTime;
//    private String acsUpdateId;
//    private Date acsUpdateTime;
//    private String deleted;


}
