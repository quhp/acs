package com.qahy.acs.hik.common.bean;

import com.alibaba.fastjson.JSON;

import java.util.HashMap;

/**
 * @Auther: lium
 * @Date: 2020/4/7 15:43
 * @Description:
 */
public class HikReturnUtil {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 状态码
     */
    private String code;

    /**
     * 状态描述
     */
    private String msg;

    /**
     * 业务对象
     */
    private Object data;


    /**
     *
     */
    public HikReturnUtil(String result) {
//        import com.alibaba.fastjson;
//        String str="";
        HashMap hashMap = JSON.parseObject(result, HashMap.class);
        this.setCode((String) hashMap.get("code"));
        this.setMsg((String) hashMap.get("msg"));
        this.setData(hashMap.get("data"));

//        buildCode();
    }
//
//    /**
//     *
//     * @param code
//     */
//    public HKReturnUtil(String code) {
//        buildCode(code);
//    }
//
//    /**
//     *
//     * @param data
//     */
//    public HKReturnUtil(Object data) {
//        buildCode();
//        buildData(data);
//    }
//
//    public HKReturnUtil(RestJson restJson) {
//        this.setCode(restJson.getCode());
//        this.setMsg(restJson.getMsg());
//    }a
//
//
//    /**
//     *
//     * @param data
//     */
//    public HKReturnUtil(Object data, String code) {
//        buildData(data, code);
//    }
//
//    /**
//     * 装载状态码
//     * @return
//     */
//    public HKReturnUtil buildCode() {
//        this.setCode("Success");
//        return buildCode("Success","操作成功");
//    }
//
//    /**
//     * 装载状态码
//     * @param status
//     * @return
//     */
//    public HKReturnUtil buildCode(String code) {
//        this.setCode("Success");
//        return this;
//    }
//
//
//
//
//    /**
//     * 装载Map对象
//     * @param data
//     * @return
//     */
//    public HKReturnUtil buildData(String key, Object value) {
//        Map<String, Object> map = new HashMap<String, Object>();
//        map.put(key, value);
//        this.data = map;
//        return this;
//    }
//
//
//    /**
//     * 装载Object对象和状态码
//     * @param data
//     * @return
//     */
//    public HKReturnUtil asObject(Object data, ReturnCode returnCode) {
//        asStatus(returnCode);
//        this.data = data;
//        return this;
//    }
//
//
//    /**
//     * 装载Page对象和状态码
//     * @param data
//     * @return
//     */
//    public HKReturnUtil asObject(IPage<?> data, ReturnCode returnCode) {
//        asStatus(returnCode);
//        this.data = data.getRecords();
//        this.pager = data;
//        return this;
//    }
//
    public String getCode() {
        return code;
    }

    public void setCode(String code) {

        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }



    @Override
    public String toString() {
        return "HKReturnUtil [code=" + code + ", msg=" + msg + ", data=" + data + "]";
    }

}
