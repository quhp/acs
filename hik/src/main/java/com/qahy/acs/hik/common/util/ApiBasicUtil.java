package com.qahy.acs.hik.common.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.qahy.basic.oa.entity.Company;
import com.qahy.basic.oa.entity.Department;
import com.qahy.acs.hik.common.base.HikConstants;
import com.qahy.basic.oa.entity.User;
import com.hikvision.artemis.sdk.ArtemisHttpUtil;
import com.hikvision.artemis.sdk.config.ArtemisConfig;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Auther: lium
 * @Date: 2020/3/30 15:25
 * @Description:
 */
@Service
public class ApiBasicUtil {


    /**
     * STEP1：设置平台参数，根据实际情况,设置host appkey appsecret 三个参数.
     */
    // artemis网关服务器ip端口


    /**
     * 调用海康接口
     * @param apiurl
     * @param body
     * @return
     */
    public  String apiUtil(String apiurl, String body) {

        ArtemisConfig.host = HikConstants.HOST;
        ArtemisConfig.appKey = HikConstants.APPKEY;
        ArtemisConfig.appSecret = HikConstants.APPSECRET;

        String previewURLsApi = HikConstants.ARTEMISPATH + apiurl;

        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", previewURLsApi);//根据现场环境部署确认是http还是https
            }
        };

        //调用接口
        String result = ArtemisHttpUtil.doPostStringArtemis(path, body, null, null, HikConstants.CONTENTTYPE , null);// post请求application/json类型参数
        return result;
    }


    /**
     *
     * 获取列表
     * @return
     *pageNo
     * pageSize
     * Type
     */
    public  String getList(String apiUrl,int pageNo,int pageSize,String type) {
        //组装请求参数
        JSONObject jsonBody = new JSONObject();
        if("door".equals(type)){
            jsonBody.put("resourceType", "door");
        }
        jsonBody.put("pageNo", pageNo);
        jsonBody.put("pageSize", pageSize);
        String body = jsonBody.toJSONString();
        return apiUtil(apiUrl,body);
    }

    /**
     *
     * 获取列表
     * @return
     *pageNo
     * pageSize
     * Type
     */
    public  String getListByParams(String apiUrl,Map<String, Object> map) {
        //组装请求参数
        JSONObject jsonBody = new JSONObject();
        for(String key:map.keySet()){//keySet获取map集合key的集合  然后在遍历key即可

//            if("pageNo".equals(key)||"pageSize".equals(key)){
//                jsonBody.put(key, (int) map.get(key));
//            }else {
                jsonBody.put(key, map.get(key));
//            }

        }

//        if("door".equals(type)){
//            jsonBody.put("resourceType", "door");
//        }
//        jsonBody.put("pageNo", pageNo);
//        jsonBody.put("pageSize", pageSize);
        String body = jsonBody.toJSONString();
        return apiUtil(apiUrl,body);
    }



    /**
     *
     * 人脸评分
     * @return
     *
     */
    public  String faceCheck(String facePicBinaryData,String facePicUrl) {

        String apiurl = "/api/frs/v1/face/picture/check";

        //组装请求参数
        JSONObject jsonBody = new JSONObject();
        jsonBody.put("facePicBinaryData", facePicBinaryData);
        jsonBody.put("facePicUrl", facePicUrl);
        String body = jsonBody.toJSONString();
        return apiUtil(apiurl,body);
    }

    /**
     *
     * 批量添加组织
     * 单次提交上限为1000条
     * @return
     *
     */
    public String orgAdds(List<Department> parms) {

        List<Map> list = new ArrayList<>();
        for(Department entity: parms){
            Map<String,Object> map = new HashMap<>();
            map.put("clientId",entity.getId().intValue() );
            map.put("orgIndexCode", entity.getDepartNo());
            map.put("orgName", entity.getDepartName());
            if(entity.getParentNo()==null ){
                entity.setParentNo("00000");
            }
            map.put("parentIndexCode", entity.getParentNo());
            map.put("orgCode", entity.getDepartNo());
            list.add(map);
        }
        JSONArray array = JSONArray.parseArray(JSON.toJSONString(list));
        String body = array.toString();
//        JSONObject jsonBody = new JSONObject();
//        String body1 = jsonBody.toJSONString();
//        String body = JSON.toJSONString(list);
        return apiUtil(HikConstants.ORG_ADDS,body);
    }

    /**
     *
     * 批量添加组织
     * 单次提交上限为1000条
     * @return
     *
     */
    public String orgAddsByCompany(List<Company> parms) {

        String apiurl = "/api/resource/v1/org/batch/add";

        List<Map> list = new ArrayList<>();
        for(Company entity: parms){
            Map<String,Object> map = new HashMap<>();
            map.put("clientId",entity.getId().intValue() );
            map.put("orgIndexCode", entity.getCompanyNo());
            map.put("orgName", entity.getName());
            if(entity.getParentId()==null ){
                entity.setParentId("root000000");
            }
            map.put("parentIndexCode", entity.getParentId());
//            map.put("orgCode", entity.getCompanyNo());
            list.add(map);
        }
        JSONArray array = JSONArray.parseArray(JSON.toJSONString(list));
        String body = array.toString();
//        String body = JSON.toJSONString(list);
        return apiUtil(HikConstants.ORG_ADDS,body);
    }

    /**
     *
     * 批量添加人员
     * 单次提交上限为1000条
     * @return
     *
     */
    public String userAdds(List<User> parms) {

        List<Map> list = new ArrayList<>();
        for(User entity: parms){
            Map<String,Object> map = new HashMap<>();
            map.put("clientId",entity.getId().intValue());
            map.put("personId", entity.getUserNo());
            map.put("personName", entity.getName());
            map.put("orgIndexCode", entity.getPartNo());
            if(entity.getGenderNo()!=null&&"10".equals(entity.getGenderNo())){
                map.put("gender", 1); //性别 1：男 2：女 0：未知
            }else if(entity.getGenderNo()!=null&&"20".equals(entity.getGenderNo())){
                map.put("gender", 2);
            }
//            map.put("birthday", "1992-09-12");
            map.put("phoneNo", entity.getMobile());
            map.put("email", entity.getEmail());
            map.put("certificateType", "990");
            map.put("certificateNo", entity.getEmpid());
            map.put("jobNo", entity.getUserNo());
            list.add(map);
        }
        String body = JSON.toJSONString(list);
        return apiUtil(HikConstants.PERSON_ADDS,body);
    }

    /**
     *
     * 批量删除人员
     * 单次提交上限为1000条
     * @return
     *
     */
    public String userDels(List<User> parms) {

        JSONObject jsonBody = new JSONObject();
        List<String> list = new ArrayList<>();
        for(User entity: parms){
            list.add(entity.getUserNo());
        }
        jsonBody.put("personIds",list);
        String body = jsonBody.toJSONString();
        return apiUtil(HikConstants.PERSON_DELETES,body);
    }

    public String getUsers(String userNo) {
        JSONObject jsonBody = new JSONObject();
        jsonBody.put("personIds", userNo);
        jsonBody.put("pageNo", 1);
        jsonBody.put("pageSize", 100);
        String body = jsonBody.toJSONString();
        return apiUtil(HikConstants.PERSON_INFO,body);
    }

    /**
     *
     * 添加人脸
     * @return
     *
     */
    public  String singleAdd(String userNo,String faceData) {

        //组装请求参数
        JSONObject jsonBody = new JSONObject();
        jsonBody.put("personId", userNo);
        jsonBody.put("faceData", faceData);
        String body = jsonBody.toJSONString();
        return apiUtil(HikConstants.SINGLE_ADD,body);
    }


    /**
     *
     * 删除人脸
     * @return
     *
     */
    public  String singleDel(String faceId) {

        //组装请求参数
        JSONObject jsonBody = new JSONObject();
        jsonBody.put("faceId", faceId);
        String body = jsonBody.toJSONString();
        return apiUtil(HikConstants.SINGLE_DELETE,body);
    }

    /**
     * 出入权限配置
     * @return
     */
    public String auth_configAdd(Map map) {

//        Map<String,String> personDatas = new HashMap<>();
//        personDatas.put("indexCodes",people.getPeopleName());
//        personDatas.put("personDataType",people.getSex());
//            Map<String,Object> resourceInfos = new HashMap<>();
//        resourceInfos.put("resourceIndexCode",people.getPeopleName());
//        resourceInfos.put("resourceType",people.getSex());
//        resourceInfos.put("channelNos",people.getSex());
//        resourceInfos.put("startTime",people.getSex());
//        resourceInfos.put("endTime",people.getSex());
        //组装请求参数
        List<Map> list1 = new ArrayList<>();
        list1.add((Map) map.get("personDatas"));
        List<Map> list2 = new ArrayList<>();
        list2.add((Map) map.get("resourceInfos"));
        JSONObject jsonBody = new JSONObject();
        jsonBody.put("personDatas", list1);
        jsonBody.put("resourceInfos", list2);
        String body = jsonBody.toJSONString();
        return apiUtil(HikConstants.AUTH_CONFIG_ADD,body);
    }

    /**
     * 出入权限配置
     * @return
     */
    public String auth_config_add(Map map) {
        JSONObject jsonBody = new JSONObject();
        jsonBody.put("personDatas", (List<Map>)map.get("personDatas"));
        jsonBody.put("resourceInfos", (List<Map>)map.get("resourceInfos"));
        String body = jsonBody.toJSONString();
        return apiUtil(HikConstants.AUTH_CONFIG_ADD,body);
    }

    /**
     * 删除权限配置
     * @return
     */
    public String auth_config_delete(Map map) {
        JSONObject jsonBody = new JSONObject();
        jsonBody.put("personDatas", (List<Map>)map.get("personDatas"));
        jsonBody.put("resourceInfos", (List<Map>)map.get("resourceInfos"));
        String body = jsonBody.toJSONString();
        return apiUtil(HikConstants.AUTH_CONFIG_DELETE,body);
    }


    /**
     * 根据出入权限配置快捷下载
     * @return
     */
    public String shortcut(int taskType ,Map map) {

        //组装请求参数
        List<Map> list = new ArrayList<>();
        list.add((Map) map.get("resourceInfos"));
        JSONObject jsonBody = new JSONObject();
        jsonBody.put("taskType", taskType);
        jsonBody.put("resourceInfos", list);
        String body = jsonBody.toJSONString();
        return apiUtil(HikConstants.CONFIG_URATION_SHORTCUT,body);
    }

    /**
     * 根据出入权限配置快捷下载
     * @return
     */
    public String config_uration_shortcut(int taskType ,List<Map> list) {

        //组装请求参数
        JSONObject jsonBody = new JSONObject();
        jsonBody.put("taskType", taskType);
        jsonBody.put("resourceInfos", list);
        String body = jsonBody.toJSONString();
        return apiUtil(HikConstants.CONFIG_URATION_SHORTCUT,body);
    }





    /**
     *
     * 单个添加人脸分组
     * @param name
     * @param description
     * @return
     *
     */
    public  String groupAddition(String name,String description) {

        String apiurl = "/api/frs/v1/face/group/single/addition";

//        /api/frs/v1/face/group/single/addition

        //组装请求参数
        JSONObject jsonBody = new JSONObject();
        jsonBody.put("name", name);
        jsonBody.put("description", description);
        String body = jsonBody.toJSONString();
        return apiUtil(apiurl,body);
    }

    /**
     *
     * h获取人脸分组
     * @param name
//     * @param description
     * @return
     *
     */
    public  String groupGet(String name) {

        String apiurl = "/api/frs/v1/face/group";

        //组装请求参数
        JSONObject jsonBody = new JSONObject();
        jsonBody.put("name", name);
        String body = jsonBody.toJSONString();
        return apiUtil(apiurl,body);
    }


    /**单个添加人脸
     * @param groupId
     * @param people
     * @return
     *
     */
//    public  String faceAddition(String groupId ,EntPeople people) {
//
//        String apiurl = "/api/frs/v1/face/single/addition";
//
//        JSONObject jsonBody = new JSONObject();
//        Map<String,String> faceInfo = new HashMap<>();
//        faceInfo.put("name",people.getPeopleName());
//        faceInfo.put("sex",people.getSex());
//        faceInfo.put("certificateType","111");
//        faceInfo.put("certificateNum",people.getPeopleCard());
//        Map<String,String> facePic = new HashMap<>();
//        facePic.put("faceUrl",people.getPeopleHeader() );
//        jsonBody.put("faceGroupIndexCode", groupId);
//        jsonBody.put("faceInfo", faceInfo);
//        jsonBody.put("facePic", facePic);
//        String body = jsonBody.toJSONString(jsonBody);
//
//        return apiUtil(apiurl,body);
//    }


    /**
     * 单个添加重点人员识别计划
     * @param name
     * @param groupIds
     * @param cameraIds
     * @param resourceIds
     * @param description
     * @param threshold
     * @param timeBlockList
     * @return
     */
    public  String blackAddition(String name ,String[] groupIds,String[] cameraIds
            ,String[] resourceIds,String description,int threshold, Map<String,Object> timeBlockList) {

        String apiurl = "/api/frs/v1/plan/recognition/black/addition";

        JSONObject jsonBody = new JSONObject();

        //时间段，不填为全天候，这里不填
//        Map<String,Object> timeBlockList = new HashMap<>();
//        Map<String,String> timeRange = new HashMap<>();
//        timeRange.put("startTime","00:00");
//        timeRange.put("endTime","23:59");
//        timeBlockList.put("dayOfWeek",2);
//        timeBlockList.put("timeRange",timeRange);

        jsonBody.put("name", name);
        jsonBody.put("faceGroupIndexCodes", groupIds);
        jsonBody.put("cameraIndexCodes", cameraIds);
        jsonBody.put("recognitionResourceIndexCodes", resourceIds);
        jsonBody.put("recognitionResourceType", "SUPER_BRAIN");
        jsonBody.put("description", description);
        jsonBody.put("threshold", threshold);

//        jsonBody.put("timeBlockList", timeBlockList);

        String body = jsonBody.toJSONString(jsonBody);

        return apiUtil(apiurl,body);
    }

    /**
     * 单个添加陌生人识别计划
     * @param name
     * @param groupIds
     * @param cameraIds
     * @param resourceIds
     * @param description
     * @param threshold
     * @param timeBlockList
     * @return
     */
    public  String whiteAddition(String name ,String[] groupIds,String[] cameraIds
            ,String[] resourceIds,String description,int threshold, Map<String,Object> timeBlockList) {

        String apiurl = "/api/frs/v1/plan/recognition/white/addition";

        JSONObject jsonBody = new JSONObject();

        JSONArray array = new JSONArray();
        jsonBody.put("name", name);
        jsonBody.put("faceGroupIndexCodes", groupIds);
        jsonBody.put("cameraIndexCodes", cameraIds);
        jsonBody.put("recognitionResourceIndexCodes", resourceIds);
        jsonBody.put("recognitionResourceType", "SUPER_BRAIN");
        jsonBody.put("description", description);
        jsonBody.put("threshold", threshold);

        String body = jsonBody.toJSONString(jsonBody);

        return apiUtil(apiurl,body);
    }



}
