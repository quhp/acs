package com.qahy.basic.common.bean;

import java.io.Serializable;

/**
 * @version 1.0
 * @Description //TODO bean基类
 * @Author jujian
 * @Date 2021/6/16 9:12
 **/
public class BaseBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
}
