package com.qahy.basic.common.base;

import org.springframework.stereotype.Component;

/**
 * @version 1.0
 * @Description //TODO 常量
 * @Author jujian
 * @Date 2021/11/9 14:39
 **/
@Component
public class Constants {

    /**
     * 请求成功code
     */
    public static String SUCCESS_CODE = "200";

    /**
     * 请求失败code
     */
    public static String ERROR_CODE = "500";

    /**
     * Content-Type json值
     */
    public static final String CONTENT_TYPE_JSON = "application/json; charset=utf-8";


}
