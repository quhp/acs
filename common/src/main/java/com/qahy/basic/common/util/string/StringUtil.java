package com.qahy.basic.common.util.string;

import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;

/**
 * 字符串工具类
 *
 * @author jujian
 */
public class StringUtil extends StringUtils {

    /**
     *
     */
    public static final String EMPTY = "";


    /**
     * 判断字符串是否为空
     *
     * @param str
     * @return
     */
    public static boolean isNotEmpty(String str) {
        return !StringUtil.isEmpty(str) && !str.trim().equals("");
    }

    /**
     * 判断字符串是否为空
     *
     * @param str
     * @return
     */
    public static boolean isNotEmpty(Object obj) {
        boolean flag = true;
        if (obj == null) {
            flag = false;
        }
        return flag;
    }


    /**
     * @param str
     * @return
     * @throws
     * @Title: getStrValue
     * @Description: 进行Null处理
     * @return: String
     */
    public static String getStrValue(String str) {
        if (StringUtil.isEmpty(str)) {
            return "";
        } else {
            return str;
        }
    }

    /**
     * 判断字符串是否为空
     *
     * @param string
     * @return
     */
    public static boolean isEmpty(String... array) {
        boolean flag = false;
        if (array != null && array.length > 0) {
            for (String str : array) {
                flag = StringUtil.isEmpty(str);
                if (flag) {
                    break;
                }
            }
        }
        return flag;
    }

    /**
     * 判断List是否为空
     *
     * @param list
     * @return
     */
    public static boolean isEmpty(List<?> list) {
        return list == null || list.size() == 0;
    }

    /**
     * 判断List是否为空
     *
     * @param list
     * @return
     */
    public static boolean isNotEmpty(List<?> list) {
        return !StringUtil.isEmpty(list);
    }

    /**
     * 判断Map是否为空
     *
     * @param list
     * @return
     */
    public static boolean isEmpty(Map<?, ?> map) {
        return map == null || map.size() == 0;
    }

    /**
     * 判断object
     *
     * @param list
     * @return
     */
    public static boolean isObjEmpty(Object obj) {
        return StringUtil.isEmpty(obj) || obj.toString().trim().equals("");
    }

    /**
     * 判断Map是否为空
     *
     * @param list
     * @return
     */
    public static boolean isNotEmpty(Map<?, ?> map) {
        return !StringUtil.isEmpty(map);
    }


    public static boolean isNotAllEmptyObjects(Object... objs) {
        boolean flag = true;
        if (objs instanceof String[]) {
            flag = StringUtil.isNotEmpty(objs);
        } else {
            for (Object obj : objs) {
                if (StringUtil.isEmpty(obj)) {
                    flag = false;
                    break;
                }


            }

        }
        return flag;

    }

}
