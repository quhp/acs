package com.qahy.basic.common.util.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @version 1.0
 * @Description //TODO 日期处理工具类
 * @Author jujian
 * @Date 2021/11/19 15:51
 **/
public class DateUtil {

    private static DateUtil date;

    public static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static SimpleDateFormat sdf_ymd = new SimpleDateFormat("yyyyMMdd");
    public static SimpleDateFormat sdf_ymd_hms = new SimpleDateFormat("yyyyMMdd HH:mm:ss");

    public static SimpleDateFormat sdf_hik = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    public static Date toDate(String date, String dateFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        try {
            return sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static long toMillisecond(String date, String dateFormat) {
        return toDate(date, dateFormat).getTime();
    }

    public static String toString(Date date, String dateFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        return sdf.format(date);
    }


    /**
     * 得到指定日期的一天的的最后时刻23:59:59
     * @param date
     * @return Date
     */
    public static Date getFinallyDate(Date date) {
        String temp = sdf_ymd.format(date);
        temp += " 23:59:59";
        try {
            return sdf_ymd_hms.parse(temp);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * 得到指定日期的一天的开始时刻00:00:00
     * @param date
     * @return Date
     */
    public static Date getStartDate(Date date) {
        String temp = sdf_ymd.format(date);
        temp += " 00:00:00";
        try {
            return sdf_ymd_hms.parse(temp);
        } catch (Exception e) {
            return null;
        }
    }
    //日期转海康格式字符串
    public static String dateToHikString(Date date) {
        String dateStr = sdf_hik.format(date);
        return dateStr;
    }

    /**
     * 返回昨天
     * @param today
     * @return
     */
    public static Date yesterday(Date today) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) - 1);
        return calendar.getTime();
    }

    /**
     * 获取昨天的时间
     * @return
     */
    public static String getYesterdayNowDate() {
        Calendar calendar = Calendar.getInstance();
        /* DAY_OF_WEEK 指示一星期中的一天 */
        calendar.set(Calendar.DAY_OF_WEEK, calendar.get(Calendar.DAY_OF_WEEK) - 1);
//        System.out.println("一天前的时间：" + format.format(calendar.getTime()));
        return format.format(calendar.getTime());
    }

    /**
     * 获取第二天的时间
     * @return
     */
    public static String beforeTomorrowToNowDate() {
        Calendar calendar = Calendar.getInstance();
        /* DAY_OF_WEEK 指示一星期中的一天 */
        calendar.set(Calendar.DAY_OF_WEEK, calendar.get(Calendar.DAY_OF_WEEK) + 1);
//        System.out.println("一天后的时间：" + format.format(calendar.getTime()));
        return format.format(calendar.getTime());
    }
}
