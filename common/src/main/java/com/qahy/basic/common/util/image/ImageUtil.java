package com.qahy.basic.common.util.image;

import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import sun.misc.BASE64Encoder;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Objects;

/**
 * @version 1.0
 * @Description //TODO
 * @Author jujian
 * @Date 2021/11/17 11:20
 **/
public class ImageUtil {

    private static String strNetImageToBase64;

//    public static void main(String[] args) {
//        //第一个:把网络图片装换成Base64
//        String netImagePath = "https:baidu.com/showPicture?picurl=L3BpYz8yZGU3NzU4YzNiNWRvLTZlbCoyMS1vOT02MTQyKjIqMmwwPTA1ODk4NzI2MTN0NT09MTIxKioqcHNiPT00MWVmKmY0NTc1MTc1MC05ZTIyNGMtM3BpMTg4bz0xMzkxOT0zNTE=";
//        //下面是网络图片转换Base64的方法
////        NetImageToBase64(netImagePath);
//
//        //下面是本地图片转换Base64的方法
//        String imagePath = "E://A04378.jpg";
////        ImageToBase64(imagePath);
//    }

    /**
     * 网络图片转换Base64的方法
     *
     * @param netImagePath
     */
    private static String NetImageToBase64(String netImagePath) {
        final ByteArrayOutputStream data = new ByteArrayOutputStream();
//        String netImageToBase64 = "";
        try {
            // 创建URL
            URL url = new URL(netImagePath);
            final byte[] by = new byte[1024];
            // 创建链接
            final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(5000);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        InputStream is = conn.getInputStream();
                        // 将内容读取内存中
                        int len = -1;
                        while ((len = is.read(by)) != -1) {
                            data.write(by, 0, len);
                        }
                        // 对字节数组Base64编码
                        BASE64Encoder encoder = new BASE64Encoder();
                        strNetImageToBase64 = encoder.encode(data.toByteArray());
                        System.out.println("网络图片转换Base64:" + strNetImageToBase64);
                        // 关闭流
                        is.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();

            return strNetImageToBase64;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return strNetImageToBase64;
    }


    /**
     * 本地图片转换Base64的方法
     *
     * @param imgPath
     */

    public static String ImageToBase64(String imgPath) {
        byte[] data = null;
        // 读取图片字节数组
        try {
            InputStream in = new FileInputStream(imgPath);
            data = new byte[in.available()];
            in.read(data);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 对字节数组Base64编码
        BASE64Encoder encoder = new BASE64Encoder();
        // 返回Base64编码过的字节数组字符串
        System.out.println("本地图片转换Base64:" + encoder.encode(Objects.requireNonNull(data)));
        return encoder.encode(Objects.requireNonNull(data));
    }

    /**
     * Smb图片转换Base64的方法
     * @param imgPath "smb://administrator:123456@192.168.3.135/gongxiang/home/A05248.jpg"
     */
    public static String SmbImageToBase64(String imgPath) {
        byte[] data = null;
        // 读取图片字节数组
        try {
            SmbFile smbFile = new  SmbFile(imgPath);
            SmbFileInputStream in = new  SmbFileInputStream(smbFile);
            data = new byte[(int) smbFile.length()];
            in.read(data);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 对字节数组Base64编码
        BASE64Encoder encoder = new BASE64Encoder();
        // 返回Base64编码过的字节数组字符串
        return encoder.encode(Objects.requireNonNull(data));
    }
}
