package com.qahy.basic.common.bean;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @version 1.0
 * @Description //TODO
 * @Author jujian
 * @Date 2021/6/16 9:14
 **/
@NoArgsConstructor
@Data
public class RestJson extends BaseBean{

    private String code;

    private String msg;

    private String desc;

    private Object data;

    public RestJson(ReturnCode returnCode) {
        this.setCode(returnCode.getCode());
        this.setDesc(returnCode.getDesc());
    }

}
