package com.qahy.basic.common.bean;

import com.qahy.basic.common.base.Constants;

/**
 * @version 1.0
 * @Description //TODO 接口返回码
 * @Author jujian
 * @Date 2021/6/10 10:52
 **/
public enum ReturnCode {

    //******************0000：返回正常状态码（httpStatus：200）******************

    /**
     * 操作成功
     */
    SUCCESS(Constants.SUCCESS_CODE, "操作成功"),


    /**
     * 应用未注册
     */
    SYS_NULL(Constants.ERROR_CODE, "应用未注册"),


    /**
     * 系统异常
     */
    SERVER_ERROR(Constants.ERROR_CODE, "系统异常"),

    /**
     * 模块异常
     */
    MODULE_ERROR(Constants.ERROR_CODE, "模块异常"),


    /**
     * 模块未找到
     */
    MODULE_NOTHAVE_ERROR(Constants.ERROR_CODE, "模块未找到"),


    /**
     * 空指针异常
     */
    NullPointerException(Constants.ERROR_CODE, "空指针异常"),

    /**
     * 更新失败
     */
    UPDATE_FAILED(Constants.ERROR_CODE, "更新失败"),


    /**
     * 导入成功
     */
    IMPORT_SUCCESS(Constants.ERROR_CODE, "导入成功"),


    /**
     * 参数错误
     */
    PARAM_FAILED(Constants.ERROR_CODE, "参数错误"),

    /**
     * 自定义错误
     */
    CUSTOM_EXCEPTION(Constants.ERROR_CODE, "自定义错误"),
    //******************1xxx：权限校验错误码******************

    /**
     * 身份验证未通过，或者验证信息失效（httpStatus：401）
     */
    UNAUTHORIZED("401", "身份验证未通过，或者验证信息失效"),

    /**
     * token是空（httpStatus：401）
     */
    TOKEN_ISNULL(Constants.ERROR_CODE, "token是空"),
    /**
     * 没有权限，服务器拒绝请求（httpStatus：403）
     */
    FORBIDDEN("403", "没有权限，服务器拒绝请求"),
    /**
     * 用户没有权限登录此系统（httpStatus：403）
     */
    FORBIDDEN_LOGIN(Constants.ERROR_CODE, "用户没有权限登录此系统"),

    /**
     * 读取文件失败
     */
    FILE_FORBIDDEN("4103", "读取文件失败"),

    /**
     * 连接池执行异常
     */
    ClientProtocolException("4104", "连接池执行异常"),

    //******************4xxx：输入校验错误码（httpStatus：400）******************

    /**
     * 类型转换错误
     */
    ClassCastException("5100", "类型转换错误"),

    /**
     * 请求失败
     */
    BAD_REQUEST(Constants.ERROR_CODE, "请求失败"),

    /**
     * 密码错误
     */
    LOGIN_FAILED(Constants.ERROR_CODE, "帐号或密码错误"),

    /**
     * 新密码与确认密码不一致
     */
    PWD_DISAGREE(Constants.ERROR_CODE, "新密码与确认密码不一致"),
    /**
     * 只能修改当前登录用户得密码
     */
    PWD_FEATED(Constants.ERROR_CODE, "只能修改当前登录用户得密码"),

    /**
     * 密码校验失败
     */
    PWD_FAILED(Constants.ERROR_CODE, "密码校验失败"),

    /**
     * 手机验证码验证失败
     */
    VERIFICATION_MESSAGE_FAIL(Constants.ERROR_CODE, "验证码错误"),

    /**
     * 原密码与新密码一致
     */
    PWD_NEWPWD_AGREES(Constants.ERROR_CODE, "原密码与新密码一致"),

    /**
     * 连接超时
     */
    SOCKET_CONNECT_TIMEOUT(Constants.ERROR_CODE, "连接超时"),

    /**
     * 用户名已存在
     */
    USERNAME_EXIST(Constants.ERROR_CODE, "用户名已存在"),


    /**
     * 用户名不存在
     */
    USERNAME_WETHER(Constants.ERROR_CODE, "用户名不存在"),

    /**
     * 用户名不能为空
     */
    USERNAME_NULL(Constants.ERROR_CODE, "用户名不能为空"),

    /**
     * 文件大小限制
     */
    MAXUPLOADSIZE_EXCEPTION(Constants.ERROR_CODE, "上传的文件大小超出限制"),
    /**
     * 文件不存在
     */
    MAXUPLOAD_EXCEPTION(Constants.ERROR_CODE, "文件不存在"),
    /**
     * 请上传文件
     */
    MAXUPUPLOAD_EXCEPTION(Constants.ERROR_CODE, "请上传文件"),

    /**
     * 更新合同推送失败
     */
    UPDATE_PACT_FAILED(Constants.ERROR_CODE, "不能删除"),

    /**
     * 此帐号密码不能修改
     */
    USER_PASSWORD_NOYUPDATE(Constants.ERROR_CODE, "此帐号密码不能修改"),

    //******************5xxx：业务异常错误码（httpStatus：740）******************

    /**
     * 新增失败
     */
    INSERT_FAILED(Constants.ERROR_CODE, "新增失败"),


    /**
     * 删除失败
     */
    DELETE_FAILED(Constants.ERROR_CODE, "删除失败"),

    //******************9xxx：系统异常错误码（httpStatus：740）******************

    /**
     * 该接口不存在
     */
    NOT_FOUND(Constants.ERROR_CODE, "该接口不存在");

    private String code;

    private String desc;

    private ReturnCode(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String toString() {
        return this.name() + "[" + this.code + "_" + desc + "]";
    }

}
