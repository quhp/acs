package com.qahy.basic.common.bean.rabbit;

import lombok.Data;

import java.io.Serializable;

/**
 * 消息bean对象.
 *
 * @author : huanglin
 * @version : 1.0
 * @since :2018/5/10 上午9:36
 */
@Data
public class Msg implements Serializable {
	
    private String id;
    private int action;
    private String msg;
    private Object data;
    private String code;
    private String from;
    private String to;
    private int status;
    private String createDate;
    private String appName;
}
