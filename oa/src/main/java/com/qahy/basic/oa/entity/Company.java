package com.qahy.basic.oa.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @Author jujian
 * @Description //TODO $todo
 * @Date  2021/11/09 16:29
 * @return
 **/
@Data
public class Company  {

    private Long id;

    private String companyNo;

    private String name;

    private String parentId;

    private String creator;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

    private String modifier;

    @JsonFormat (pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date modifyTime;

    private String remark;

    private Short state;

}
