package com.qahy.basic.oa.entity;

import lombok.Data;

/**
 * @version 1.0
 * @Description //TODO
 * @Author jujian
 * @Date 2021/11/30 14:13
 **/
@Data
public class User {


    private Long id;

    private String ehrUid;
    private String empid;
    private String userNo;
    private String name;
    private String genderNo;
    private String genderName;
    private String departmentId;
    private String partNo;
    private String department;
    private String email;
    private String companyId;
    private String companyNo;
    private String companyName;
    private String leaveTypeNo;
    private String leaveTypeName;
    private String mobile;
    private String is_leave;
    private String pid;
    private String state;
    private String creator;
    private String creatorName;
    private String modifier;
    private String modifierName;
    private String createTime;
    private String modifyTime;
    private String status;
    private String token;
    private String comeDate;
}
