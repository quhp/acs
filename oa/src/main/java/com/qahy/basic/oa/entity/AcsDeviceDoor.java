package com.qahy.basic.oa.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * @version 1.0
 * @Description //TODO
 * @Author jujian
 * @Date 2021/11/26 9:21
 **/
@Data
public class AcsDeviceDoor {

    private Integer id;
    private String type;
    private String devCode;
    private String name;
    private String region;
    private String status;
    private String deleted;
    private String permissionType;
    private String devType;

    @TableField(exist = false)
    private String userNo;
}
