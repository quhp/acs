package com.qahy.basic.oa.entity;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @Author jujian
 * @Description //TODO $todo
 * @Date  2021/11/09 16:29
 * @return
 **/
@Data
public class Department{

    private Long id;

    private String departNo;

    private String parentNo;

    private String partLevel;

    private String departName;

    private String companyNo;

    @JsonFormat (pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date cancelDate;

    private Long companyId;

    private String creator;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

    private String modifier;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date modifyTime;

    private Short state;
}
