# acs

#### 介绍
门禁系统
该系统集成海康门禁、中控门禁、ehr。
系统包含门禁管理、人员管理、人员照片管理、权限下发、门禁事件记录等功能
#### 软件架构
软件架构说明
spring cloud 2020.0.4
spring boot 2.4.12
mybatis-plus 3.4.2

#### 安装教程

1.  海康开放平台
    1.1  需在海康开放平台安装api网关，获取海康的APPKEY、APPSECRET以供系统接口调用;
    1.2  需要在海康平台添加门禁，然后系统同步信息至acs（海康不提供添加设备的接口）；
    
2.  中控系统
3.  ehr

#### 使用说明

1.  员工入职填写信息。同步至门禁系统，并根据门禁设置赋权
2.  员工通过oa申请门禁权限。该系统赋权
3.  考勤数据等事件产生数据通过定时配置同步
4.  人员离职，删除门禁系统中的数据

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
