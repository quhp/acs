package com.qahy.acs.common.bean;

import com.baomidou.mybatisplus.core.metadata
        .IPage;
import com.qahy.basic.common.bean.BaseBean;
import com.qahy.basic.common.bean.RestJson;
import com.qahy.basic.common.bean.ReturnCode;

import java.util.HashMap;
import java.util.Map;

/**
 * @version 1.0
 * @Description //TODO 接口返回
 * @Author jujian
 * @Date 2021/6/24 9:47
 **/

public class ReturnBody extends BaseBean {

    private static final long serialVersionUID = 1L;

    /**
     * 状态码
     */
    private String code;

    /**
     * 状态描述
     */
    private String desc;

    /**
     * 业务对象
     */
    private Object data;

    /**
     * 分页Bean
     */
    private IPage pager;

    public String getCode() {
        return code;
    }

    public void setCode(String code) { this.code = code; }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }


    public IPage getPager() {
        return pager;
    }

    public void setPager(IPage pager) {
        this.pager = pager;
    }

    @Override
    public String toString() {
        return "ReturnBody [code=" + code + ", desc=" + desc + ", data=" + data + ", pager=" + pager + "]";
    }



    public ReturnBody() {
        asStatus();
    }

    /**
     * 装载状态码(默认状态码)
     */
    public ReturnBody asStatus() {
        return asStatus(ReturnCode.SUCCESS);
    }

    /**
     * @param data
     */
    public ReturnBody(Object data) {
        asStatus();
        asObject(data);
    }

    public ReturnBody(RestJson restJson) {
        this.setCode(restJson.getCode());
        this.setDesc(restJson.getDesc());
    }

    /**
     * @param data
     */
    public ReturnBody(IPage<?> data) {
        asStatus();
        asObject(data);
    }

    /**
     * @param data
     */
    public ReturnBody(Object data, ReturnCode code) {
        asObject(data, code);
    }

    /**
     * 装载状态码
     * @param returnCode
     * @return
     */
    public ReturnBody asStatus(ReturnCode returnCode) {
        this.setCode(returnCode.getCode());
        this.setDesc(returnCode.getDesc());
        return this;
    }

    /**
     * 装载Object对象
     * @param data
     * @return
     */
    public ReturnBody asObject(Object data) {
        if (data instanceof IPage) {
            asObject((IPage<?>) data);
        } else {
            this.data = data;
        }
        return this;
    }

    /**
     * 装载Map对象
     * @param key
     * @param value
     * @return
     */
    public ReturnBody asObject(String key, Object value) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put(key, value);
        this.data = map;
        return this;
    }

    /**
     * 装载Page对象
     * @param IPage
     * @return
     */
    public ReturnBody asObject(IPage<?> IPage) {
        this.data = IPage.getRecords();
        IPage.setRecords(null);
        this.pager = IPage;
        return this;
    }

    /**
     * 装载Object对象和状态码
     * @param data
     * @return
     */
    public ReturnBody asObject(Object data, ReturnCode returnCode) {
        asStatus(returnCode);
        if (data instanceof IPage) {
            asObject((IPage<?>) data, returnCode);
        } else {
            this.data = data;
        }
        return this;
    }


    /**
     * 装载Page对象和状态码
     * @param data
     * @return
     */
    public ReturnBody asObject(IPage<?> data, ReturnCode returnCode) {
        asStatus(returnCode);
        this.data = data.getRecords();
        this.pager = data;
        return this;
    }
}
