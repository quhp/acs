package com.qahy.acs.common.bean;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;

/**
 * @version 1.0
 * @Description //TODO 接口返回
 * @Author jujian
 * @Date 2021/6/24 9:47
 **/
public class ReturnEntity extends ResponseEntity<ReturnBody> {

    private static final long serialVersionUID = 1L;

    public ReturnEntity(HttpStatus status) {
        super(status);
    }

    public ReturnEntity(ReturnBody body, HttpStatus status) {
        super(body, status);
    }

    public ReturnEntity(MultiValueMap<String, String> headers, HttpStatus status) {
        super(headers, status);
    }

    public ReturnEntity(ReturnBody body, MultiValueMap<String, String> headers, HttpStatus status) {
        super(body, headers, status);
    }

    public ReturnEntity(ReturnBody body) {
        this(body, asStatus(body));
    }

    private static HttpStatus asStatus(ReturnBody body) {
        if (body.getCode().startsWith("0") || body.getCode().startsWith("6") || body.getCode().startsWith("2")) {
            return HttpStatus.OK;
        } else if (body.getCode().startsWith("1")) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } else if (body.getCode().startsWith("4")) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.INTERNAL_SERVER_ERROR;

    }
}
