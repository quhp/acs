package com.qahy.acs.common.listener;

import com.qahy.basic.common.bean.rabbit.Msg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.stereotype.Component;

@Component
@Slf4j
//@RabbitListener(queues = RabbitContants.QUEUE_DEMO)//监听的队列名称 sendMsg
public class DirectReceiver {

    @RabbitHandler
    public void process(Msg msg)   {
        try {
            System.out.println("消费者收到消息  : " + msg);
        } catch (Exception e) {
           log.error("消息出现问题{}",msg);
        }
    }

}