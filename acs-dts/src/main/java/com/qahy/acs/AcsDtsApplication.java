package com.qahy.acs;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
//@SpringBootApplication
@MapperScan("com.qahy.acs.mapper.*")
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class AcsDtsApplication {

    public static void main(String[] args) {
        SpringApplication.run(AcsDtsApplication.class, args);
    }

}
