package com.qahy.acs.controller.oa;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qahy.acs.common.bean.ReturnBody;
import com.qahy.acs.common.bean.ReturnEntity;
import com.qahy.acs.service.oa.DeviceService;
import com.qahy.basic.common.base.Constants;
import com.qahy.basic.oa.entity.AcsDeviceDoor;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @version 1.0
 * @Description //TODO
 * @Author jujian
 * @Date 2021/11/26 9:55
 **/
@Api("设备管理")
//@Transactional(rollbackFor = Exception.class)
@RestController
@RequestMapping(value = "/api/device", produces = {Constants.CONTENT_TYPE_JSON})
public class DeviceController {

    @Autowired
    private DeviceService service;


    @ApiOperation(value = "获取分页列表")
    @GetMapping(value = "/queryPage")
//    @Permission("dev:page")
    public ReturnEntity queryPage(Page<AcsDeviceDoor> page, AcsDeviceDoor entity)   {

        return new ReturnEntity(new ReturnBody(service.queryPage(page,entity)));
    }


    @ApiOperation(value = "获取所有列表")
    @GetMapping(value = "/queryList")
//    @Permission("dev:list")
    public ReturnEntity queryList(AcsDeviceDoor entity)   {
        return new ReturnEntity(new ReturnBody(service.queryList(entity)));
//        return service.queryList(entity);
    }

    @ApiOperation(value = "获取所有列表")
    @GetMapping(value = "/getInfo")
//    @Permission("dev:list")
    public ReturnEntity getInfo(int id)   {
        return new ReturnEntity(new ReturnBody(service.getInfo(id)));
//        return service.queryList(entity);
    }

}
