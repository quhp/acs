package com.qahy.acs.controller.task;

import com.qahy.acs.common.bean.ReturnEntity;
import com.qahy.acs.service.task.SynTaskService;
import com.qahy.basic.common.base.Constants;
import com.qahy.basic.oa.entity.Department;
import com.qahy.basic.oa.entity.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @version 1.0
 * @Description //TODO 手动同步任务
 * @Author jujian
 * @Date 2021/11/10 14:32
 **/
@Api("手动同步")
//@Transactional(rollbackFor = Exception.class)
@RestController
@RequestMapping(value = "/api/task", produces = {Constants.CONTENT_TYPE_JSON})
public class SynTaskController {


    @Autowired
    private SynTaskService service;


    @ApiOperation(value = "同步组织")
    @PostMapping(value = "/synorg")
//    @Permission("res:page")
    public ReturnEntity synOrg(Department dep)   {
        return service.synOrg(dep);
    }


    @ApiOperation(value = "同步组织（所有）")
    @PostMapping(value = "/synorgall")
//    @Permission("res:page")
    public ReturnEntity synOrgAll()   {
        return service.synOrgAll();
    }


    @ApiOperation(value = "同步组织（子公司）")
    @PostMapping(value = "/syncompany")
//    @Permission("res:page")
    public ReturnEntity synCompany()   {
        return service.synCompany();
    }

    @ApiOperation(value = "同步人员")
    @PostMapping(value = "/synemployeeall")
//    @Permission("res:page")
    public ReturnEntity synEmployeeAll(User entity)   {
        return service.synUser(entity);
    }

    @ApiOperation(value = "同步人员人脸照片")
    @PostMapping(value = "/synsingle")
//    @Permission("res:page")
    public ReturnEntity synSingle(User entity)   {
        return service.synSingle(entity);
    }

    @ApiOperation(value = "权限配置")
    @PostMapping(value = "/synpermissionconf")
//    @Permission("res:page")
    public ReturnEntity synPermissionConf(String type, String indexCode,String devicode)   {
        String str[] = indexCode.split(",");
        if("person".equals(type)){
//            Employee entity = new Employee();
//            entity.setEmployeeNo(indexCode);
            return service.synPermissionByUser(Arrays.asList(str),devicode);
        }else if("org".equals(type)){
//            Department  entity = new Department();
//            entity.setDepartNo(indexCode);
            return service.synPermissionByOrg(Arrays.asList(str),devicode);
        }else {

        }
        return null;

    }

    @ApiOperation(value = "下载配置")
    @PostMapping(value = "/synconf")
//    @Permission("res:page")
    public ReturnEntity synConf(String indexCode)   {
        return service.synConf(indexCode);
    }


    @ApiOperation(value = "海康同步设备到acs")
    @PostMapping(value = "/syndevicetoacs")
//    @Permission("res:page")
    public ReturnEntity synDeviceToAcs(String type)   {

        return service.synDeviceToAcs(type);
    }

    @ApiOperation(value = "海康同事件到acs")
    @PostMapping(value = "/syneventsdoortoacstest")
//    @Permission("res:page")
    public ReturnEntity synEventsDoorToAcsTest()   {

//        return service.synDeviceToAcs();
        return null;
    }

    @ApiOperation(value = "海康同事件到acs")
    @PostMapping(value = "/syneventsdoortoacs")
//    @Permission("res:page")
    public ReturnEntity synEventsDoorToAcs()   {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("pageNo",1);
        return service.synEventsDoorToAcs(map);
    }

    @PostMapping(value = "/synUserInduction")
//    @Permission("res:page")
    public ReturnEntity synUserInduction()   {
        service.synUserInduction();
        return null;
    }

    @PostMapping(value = "/synUserQuit")
//    @Permission("res:page")
    public ReturnEntity synUserQuit()   {
        service.synUserQuit();
        return null;
    }


    @PostMapping(value = "/synUser1")
//    @Permission("res:page")
    public ReturnEntity synUser1(int type)   {
        service.synUser1(type);
        return null;
    }



}
