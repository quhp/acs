package com.qahy.acs.controller.oa;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qahy.acs.common.bean.ReturnBody;
import com.qahy.acs.common.bean.ReturnEntity;
import com.qahy.acs.service.oa.OpsService;
import com.qahy.basic.common.base.Constants;
import com.qahy.basic.oa.entity.AcsDeviceDoor;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @version 1.0
 * @Description //TODO
 * @Author jujian
 * @Date 2021/11/26 9:49
 **/
@Api("运维管理")
//@Transactional(rollbackFor = Exception.class)
@RestController
@RequestMapping(value = "/api/ops", produces = {Constants.CONTENT_TYPE_JSON})
public class OpsController {

    @Autowired
    private OpsService service;

    @ApiOperation(value = "给门添加人员")
    @PostMapping(value = "/doorLoaToEmployee")
//    @Permission("dev:page")
    public ReturnEntity doorLoaToEmployee(Page<AcsDeviceDoor> page, AcsDeviceDoor entity)   {
        return null ;
//        return new ReturnEntity(new ReturnBody(service.queryPage(page,entity)));
    }

    @ApiOperation(value = "给人添加门禁")
    @PostMapping(value = "/employeeLoaToDoor")
//    @Permission("dev:page")
    public ReturnEntity employeeLoaToDoor(Page<AcsDeviceDoor> page, AcsDeviceDoor entity)   {
        return null ;
//        return new ReturnEntity(new ReturnBody(service.queryPage(page,entity)));
    }

    @ApiOperation(value = "给人添加门禁")
    @PostMapping(value = "/limitsOfAuthority")
//    @Permission("dev:page")
    public ReturnEntity limitsOfAuthority(String userNo,String ids)   {

        ReturnBody iii = service.openLimit(userNo,ids);
        return new ReturnEntity(service.openLimit(userNo,ids)) ;
//        return new ReturnEntity(new ReturnBody(service.queryPage(page,entity)));
    }

    @ApiOperation(value = "给人删除门禁")
    @PostMapping(value = "/outlimists")
//    @Permission("dev:page")
    public ReturnEntity outLimists(String userNo,String ids)   {
        return new ReturnEntity(service.outLimit(userNo,ids)) ;
    }

}
