package com.qahy.acs.controller.oa;

import com.qahy.acs.common.bean.ReturnBody;
import com.qahy.acs.common.bean.ReturnEntity;
import com.qahy.acs.service.dongbao.EhrImageService;
import com.qahy.acs.service.oa.HrService;
import com.qahy.basic.common.base.Constants;
import com.qahy.basic.oa.entity.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @version 1.0
 * @Description //TODO
 * @Author jujian
 * @Date 2021/12/1 9:14
 **/
@Api("HR管理")
//@Transactional(rollbackFor = Exception.class)
@RestController
@RequestMapping(value = "/api/hr", produces = {Constants.CONTENT_TYPE_JSON})
public class HrController {

    @Autowired
    private HrService service;

    @Autowired
    private EhrImageService imageService;

    @ApiOperation(value = "员工入职")
    @PostMapping(value = "/user/induction")
//    @Permission("dev:page")
    public ReturnEntity userInduction(String userNo)   {
        return new ReturnEntity(new ReturnBody(service.userInduction(userNo))) ;
    }

    @ApiOperation(value = "员工离职")
    @PostMapping(value = "/user/quit")
//    @Permission("dev:page")
    public ReturnEntity userQuit(String userNo)   {

        return new ReturnEntity(new ReturnBody(service.userQuit(userNo))) ;
    }

    @ApiOperation(value = "同步打卡记录")
    @PostMapping(value = "/user/clockin")
//    @Permission("dev:page")
    public ReturnEntity userClockin(String userName, Date startTime, Date endTime)   {
        return new ReturnEntity(new ReturnBody(service.userClockin(userName,startTime,endTime))) ;
    }

    @PostMapping(value = "/user/initialization")
//    @Permission("dev:page")
    public ReturnEntity userInitialization(int type)   {
        return new ReturnEntity(service.userInitialization(type)) ;
    }



    @GetMapping(value = "/user/imageFixName")
//    @Permission("dev:page")
    public ReturnEntity imageFixName(User user)   {
        imageService.FixFileName(user);
        return new ReturnEntity(new ReturnBody("")) ;
    }




}
