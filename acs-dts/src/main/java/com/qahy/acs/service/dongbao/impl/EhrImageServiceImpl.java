package com.qahy.acs.service.dongbao.impl;

import com.qahy.acs.service.oa.UserService;
import com.qahy.acs.service.dongbao.EhrImageService;
import com.qahy.basic.common.util.image.FileUtil;
import com.qahy.basic.oa.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @version 1.0
 * @Description //TODO
 * @Author jujian
 * @Date 2021/12/8 11:16
 **/
@Service
public class EhrImageServiceImpl implements EhrImageService {

    @Autowired
    private UserService userService;


    @Override
    public void FixFileName(User user1) {
        List<User> users = userService.getList(user1);
        for(User user:users){
            FileUtil.FixFileName("E://images/cy/"+user.getUserNo()+".jpg",user.getEmpid()+"_20");
        }
    }
}
