package com.qahy.acs.service.oa;

import com.qahy.acs.common.bean.ReturnBody;

/**
 * @version 1.0
 * @Description //TODO $
 * @Author jujian
 * @Date 2021/11/26 11:57
 **/
public interface OpsService {
    ReturnBody openLimit(String userNo, String ids);

    ReturnBody outLimit(String userNo, String ids);
//    CollectionUtils.isEmpty
}
