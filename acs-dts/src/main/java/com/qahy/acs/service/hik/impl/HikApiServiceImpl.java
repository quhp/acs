package com.qahy.acs.service.hik.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.qahy.acs.common.bean.ReturnBody;
import com.qahy.acs.hik.common.base.HikConstants;
import com.qahy.acs.hik.common.bean.HikReturnUtil;
import com.qahy.acs.hik.common.util.ApiBasicUtil;
import com.qahy.acs.hik.entity.HikDeviceAcs;
import com.qahy.acs.hik.entity.HikDeviceDoor;
import com.qahy.acs.hik.entity.HikEventsDoor;
import com.qahy.acs.mapper.hik.HikDeviceAcsMapper;
import com.qahy.acs.mapper.hik.HikDeviceDoorMapper;
import com.qahy.acs.mapper.hik.HikEventsDoorMapper;
import com.qahy.acs.service.dongbao.AttCardDataService;
import com.qahy.acs.service.oa.CompanyService;
import com.qahy.acs.service.oa.DepartmentService;
import com.qahy.acs.service.oa.DeviceService;
import com.qahy.acs.service.oa.UserService;
import com.qahy.acs.service.hik.HikApiService;
import com.qahy.basic.common.util.date.DateUtil;
import com.qahy.basic.common.util.image.ImageUtil;
import com.qahy.basic.common.util.string.StringUtil;
import com.qahy.basic.oa.entity.AcsDeviceDoor;
import com.qahy.basic.oa.entity.Company;
import com.qahy.basic.oa.entity.Department;
import com.qahy.basic.oa.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @version 1.0
 * @Description //TODO
 * @Author jujian
 * @Date 2021/11/26 14:20
 **/
@Service
public class HikApiServiceImpl implements HikApiService {

    @Autowired
    private ApiBasicUtil hkService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private DepartmentService depService;
    @Autowired
    private UserService userService;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private HikDeviceAcsMapper deviceAcsMapper;
    @Autowired
    private HikDeviceDoorMapper deviceDoorMapper;
    @Autowired
    private HikEventsDoorMapper eventsDoorMapper;

    @Autowired
    private AttCardDataService cardDataService;

    @Override
    public ReturnBody addDepartment(String departmentNo) {
        List<Department>  list = depService.getByCode(departmentNo);
        String result = hkService.orgAdds(list);
        HikReturnUtil rethk = new HikReturnUtil(result);
        return new ReturnBody(rethk);
    }

    @Override
    public ReturnBody addCompany(String companyNo) {

        List<Company> list = companyService.getByCode(companyNo);
        String result = hkService.orgAddsByCompany(list);
        HikReturnUtil rethk = new HikReturnUtil(result);
        return new ReturnBody(rethk);
    }

    @Override
    public ReturnBody addUser(String userNo) {
        List<User> list = userService.getByNo(userNo);
        String result = hkService.userAdds(list);
        HikReturnUtil rethk = new HikReturnUtil(result);
        return new ReturnBody(rethk);
    }

    @Override
    public ReturnBody deleteUser(String userNo) {
        List<User> list = userService.getByNo(userNo);
        String result = hkService.userDels(list);
        HikReturnUtil rethk = new HikReturnUtil(result);
        return new ReturnBody(rethk);
    }

    @Override
    public ReturnBody addSingle(String userNo) {
        List<User> list = userService.getByNo(userNo);
        String result = "";
        for(User info : list){
            try {
                result = hkService.singleAdd(info.getUserNo(), ImageUtil.ImageToBase64("E://A04581//cy_acs//images//"+info.getUserNo()+".jpg"));
            }catch (Exception e) {
            }
        }
        HikReturnUtil rethk = new HikReturnUtil(result);
        return new ReturnBody(rethk);
    }

    @Override
    public ReturnBody deleteSingle(String userNo) {
        String userResult = hkService.getUsers(userNo);
        HikReturnUtil userInfo = new HikReturnUtil(userResult);
        if(!"0".equals(userInfo.getCode())){
            return new ReturnBody(userInfo);
        }
        JSONObject jdata = (JSONObject) userInfo.getData();
        int total = Integer.parseInt(jdata.getString("total"));
        if(total==0){
            return new ReturnBody("无人员信息");
        }
        List<JSONObject> plist = (List<JSONObject>)jdata.get("list");
//        if(plist!=null&&plist.size()>0){
        for(JSONObject pinfo : plist){
            List<JSONObject> photos = (List<JSONObject>)pinfo.get("personPhoto");
            if(photos==null||photos.size()==0){
                return new ReturnBody("无人脸信息");
            }else {
                for(JSONObject photo : photos){
                    String faceId = photo.getString("personPhotoIndexCode");
                    String fResult = hkService.singleDel( faceId);
                    HikReturnUtil faceR = new HikReturnUtil(fResult);
                    if(!"0".equals(faceR.getCode())){
                        return new ReturnBody(faceR);
                    }
                }
            }
        }
//        }
        return new ReturnBody("操作成功！");
    }

    @Override
    public ReturnBody addPermission(String userNo, String deviceCode) {
        Map map = new HashMap();
        Map<String,Object> personDatas = new HashMap<>();
        personDatas.put("personDataType","person");
        personDatas.put("indexCodes",userNo);
        Map<String,Object> resourceInfos = new HashMap<>();
        resourceInfos.put("resourceIndexCode",deviceCode);
        resourceInfos.put("resourceType","door");
        map.put("personDatas",personDatas);
        map.put("resourceInfos",resourceInfos);
        String result =  hkService.auth_configAdd(map);
        HikReturnUtil rethk = new HikReturnUtil(result);
        return new ReturnBody(rethk);
    }

    @Override
    public ReturnBody authConfigAdd(List<String> userNos, List<String> deviceCodes) {
        Map map = new HashMap();
        List<Map> personDatas = new ArrayList<>();
        List<Map> resourceInfos = new ArrayList<>();
        for(String userNo:userNos){
            Map<String,Object> personData = new HashMap<>();
            personData.put("personDataType","person");
            personData.put("indexCodes",userNo);
            personDatas.add(personData);
        }
        for(String deviceCode:deviceCodes){
            Map<String,Object> resourceInfo = new HashMap<>();
            resourceInfo.put("resourceIndexCode",deviceCode);
            resourceInfo.put("resourceType","door");
            resourceInfos.add(resourceInfo);
        }
        map.put("personDatas",personDatas);
        map.put("resourceInfos",resourceInfos);
        String result =  hkService.auth_config_add(map);
        HikReturnUtil rethk = new HikReturnUtil(result);
        return new ReturnBody(rethk);
    }

    @Override
    public ReturnBody authConfigDelete(List<String> userNos, List<String> deviceCodes) {
        Map map = new HashMap();
        List<Map> personDatas = new ArrayList<>();
        List<Map> resourceInfos = new ArrayList<>();
        List<String > indexCodes = new ArrayList<>();
//        for(String userNo:userNos){
        Map<String,Object> personData = new HashMap<>();
        personData.put("personDataType","person");
        personData.put("indexCodes",userNos);
        personDatas.add(personData);
//        }
        for(String deviceCode:deviceCodes){
            Map<String,Object> resourceInfo = new HashMap<>();
            resourceInfo.put("resourceIndexCode",deviceCode);
            resourceInfo.put("resourceType","door");
            resourceInfos.add(resourceInfo);
        }
        map.put("personDatas",personDatas);
        map.put("resourceInfos",resourceInfos);
        String result =  hkService.auth_config_delete(map);
        HikReturnUtil rethk = new HikReturnUtil(result);
        return new ReturnBody(rethk);
    }



    @Override
    public ReturnBody confPermission(String deviceCode) {
        Map map = new HashMap();
        Map<String,Object> resourceInfos = new HashMap<>();
        resourceInfos.put("resourceIndexCode",deviceCode);
        resourceInfos.put("resourceType","door");
        map.put("resourceInfos",resourceInfos);
        String result =  hkService.shortcut(4,map);
        HikReturnUtil rethk = new HikReturnUtil(result);
        return new ReturnBody(rethk);
    }

    /***
     * @Author jujian
     * @Description //TODO $todo
     * @Date  2021/12/1 11:29
     * @param taskType 4:下发；5：删除
     * @return
     **/
    @Override
    public ReturnBody configUrationShortcut(int taskType,List<String> deviceCodes) {
        Map map = new HashMap();
        List<Map> resourceInfos = new ArrayList<>();
        for(String deviceCode:deviceCodes){
            Map<String,Object> resourceInfo = new HashMap<>();
            resourceInfo.put("resourceIndexCode",deviceCode);
            resourceInfo.put("resourceType","door");
            resourceInfos.add(resourceInfo);
        }
        String result =  hkService.config_uration_shortcut(taskType,resourceInfos);
        HikReturnUtil rethk = new HikReturnUtil(result);
        return new ReturnBody(rethk);
    }

    @Override
    public ReturnBody deletePermission(String deviceCode) {
        Map map = new HashMap();
        Map<String,Object> resourceInfos = new HashMap<>();
        resourceInfos.put("resourceIndexCode",deviceCode);
        resourceInfos.put("resourceType","door");
        map.put("resourceInfos",resourceInfos);
        String result =  hkService.shortcut(5,map);
        HikReturnUtil rethk = new HikReturnUtil(result);
        return new ReturnBody(rethk);
    }


    @Override
    public ReturnBody synDeviceToAcs(String type) {
        if(StringUtil.isEmpty()){

        }else if("door".equals(type)){
            synDeviceDoorToAcs();
        }else if("acsDevice".equals(type)){
            synDeviceAcsToAcs();
        }else if("reader".equals(type)){

        }
        return null;
    }


    public ReturnBody synDeviceAcsToAcs() {
        String result = hkService.getList(HikConstants.DEVEICE_ASC_URL,1,1000,"");
        HikReturnUtil rethk = new HikReturnUtil(result);
        if("0".equals(rethk.getCode())){
            Object data = rethk.getData();
            List<Object> list = (List<Object>)((Map)data).get("list");
            for(Object info:list){
                String mapStr= JSON.toJSONString((Map)info);//-->Map集合转换成JSON字符串
                HikDeviceAcs entity = JSON.parseObject(mapStr,HikDeviceAcs.class);
                entity.setId(entity.getIndexCode());
                entity.setAcsCreateId("sys");
                entity.setAcsCreateTime(new Date());
                entity.setAcsUpdateId("sys");
                entity.setAcsUpdateTime(new Date());
                entity.setDeleted("0");
                entity.setOnLine("0");
                entity.setStatus("0");
                try {
                    deviceAcsMapper.insert(entity);
                }catch (Exception e) {

                }

            }
//            }
        }
        return null;
    }

    public ReturnBody synDeviceDoorToAcs() {
        String result = hkService.getList(HikConstants.DEVEICE_DOOR_URL,1,1000,"");
        HikReturnUtil rethk = new HikReturnUtil(result);
        if("0".equals(rethk.getCode())){
            Object data = rethk.getData();
            List<Object> list = (List<Object>)((Map)data).get("list");
            for(Object info:list){
                String mapStr= JSON.toJSONString((Map)info);
                HikDeviceDoor entity = JSON.parseObject(mapStr,HikDeviceDoor.class);
                entity.setId(entity.getIndexCode());
                entity.setAcsCreateId("sys");
                entity.setAcsCreateTime(new Date());
                entity.setAcsUpdateId("sys");
                entity.setAcsUpdateTime(new Date());
                entity.setDeleted("0");
                entity.setOnLine("0");
                entity.setStatus("0");
                try {
                    deviceDoorMapper.insert(entity);
                }catch (Exception e) {
                }
            }
        }
        return null;
    }

    @Override
    public HikReturnUtil eventsDoor(Map<String, Object> map) {
        int pageSize = 1000;
        //        map.put("pageNo",pageNo);
        map.put("pageSize",pageSize);
        String dateStart = DateUtil.dateToHikString(DateUtil.getStartDate(DateUtil.yesterday(new Date())));
        String dateEnd = DateUtil.dateToHikString(DateUtil.getFinallyDate(DateUtil.yesterday(new Date())));
        map.put("startTime",dateStart);
        map.put("endTime",dateEnd);
        map.put("receiveStartTime",dateStart);
        map.put("receiveEndTime",dateEnd);
        String result = hkService.getListByParams(HikConstants.EVENTS_DOOR,map);
        HikReturnUtil rethk = new HikReturnUtil(result);
        return rethk;

    }

    public List<String> doorIndexCodes(int type){
        AcsDeviceDoor doorparams = new AcsDeviceDoor();
        doorparams.setType("1");
        doorparams.setDevType("1");
        List<AcsDeviceDoor> doorList = deviceService.queryList(doorparams);
        return null;
    }

    @Override
    public ReturnBody synEventsDoorToAcs(Map<String, Object> map) {
        int pageNo = (int) map.get("pageNo");
        HikReturnUtil rethk = eventsDoor(map);
        if("0".equals(rethk.getCode())){
            Object data = rethk.getData();
            List<Object> list = (List<Object>)((Map)data).get("list");
            for(Object info:list){
                String mapStr= JSON.toJSONString((Map)info);
                HikEventsDoor entity = JSON.parseObject(mapStr, HikEventsDoor.class);
                entity.setId(entity.getEventId());
                entity.setAcsCreateId("sys");
                entity.setAcsCreateTime(new Date());
                entity.setAcsUpdateId("sys");
                entity.setAcsUpdateTime(new Date());
                entity.setDeleted("0");
                entity.setStatus("0");
                try {
                    eventsDoorMapper.insert(entity);
                }catch (Exception e) {
                }
            }
            if((int)((Map)data).get("totalPage") > pageNo ){
                map.put("pageNo",pageNo+1);
                synEventsDoorToAcs(map);
            }

        }
        return null;
    }

    @Override
    public ReturnBody synEventsDoorToEhr(Map<String, Object> map) {

        int pageNo = (int) map.get("pageNo");
        HikReturnUtil rethk = eventsDoor(map);
        if("0".equals(rethk.getCode())){
            Object data = rethk.getData();
            List<Object> list = (List<Object>)((Map)data).get("list");
            for(Object info:list){
                String mapStr= JSON.toJSONString((Map)info);
                HikEventsDoor entity = JSON.parseObject(mapStr, HikEventsDoor.class);
                try {
                    cardDataService.saveRecord(entity);
                }catch (Exception e) {
                }
            }
            if((int)((Map)data).get("totalPage") > pageNo ){
                map.put("pageNo",pageNo+1);
                synEventsDoorToEhr(map);
            }

        }
        return null;
    }


    @Override
    public ReturnBody synUserClockin(Map<String, Object> map) {
        int pageNo = (int) map.get("pageNo");
        String result = hkService.getListByParams(HikConstants.EVENTS_DOOR,map);
        HikReturnUtil rethk = new HikReturnUtil(result);
        if("0".equals(rethk.getCode())){
            Object data = rethk.getData();
            List<Object> list = (List<Object>)((Map)data).get("list");
            for(Object info:list){
                String mapStr= JSON.toJSONString((Map)info);
                HikEventsDoor entity = JSON.parseObject(mapStr, HikEventsDoor.class);
                try {
                    int i = cardDataService.saveRecord(entity);
                    System.out.println(i);
                }catch (Exception e) {

                }
            }
            if((int)((Map)data).get("totalPage") > pageNo ){
                map.put("pageNo",pageNo+1);
                synUserClockin(map);
            }

        }
        return null;
    }










    @Override
    public ReturnBody deletePermissions(List<AcsDeviceDoor> doors) {
        List<String> doorList = new ArrayList<>();
        for(AcsDeviceDoor door :doors){
//            if("1".equals(door.getType()))
            doorList.add(door.getDevCode());
        }
        return this.configUrationShortcut(5,doorList);
    }

    @Override
    public ReturnBody savePermission(String userNo,List<AcsDeviceDoor> doors){
        List<String> doorList = new ArrayList<>();
        for(AcsDeviceDoor door :doors){
            doorList.add(door.getDevCode());
        }
        List<String> userList = new ArrayList<>();
        userList.add(userNo);
        ReturnBody addResult = authConfigAdd(userList, doorList);
        if(!"200".equals(addResult.getCode())){
            return addResult;
        }
        return configUrationShortcut(4,doorList);
    }


    @Override
    public ReturnBody outPermission(String userNo,List<AcsDeviceDoor> doors){
        List<String> doorList = new ArrayList<>();
        for(AcsDeviceDoor door :doors){
            doorList.add(door.getDevCode());
        }
        List<String> userList = new ArrayList<>();
        userList.add(userNo);
        ReturnBody addResult = authConfigDelete(userList, doorList);
        if(!"200".equals(addResult.getCode())){
            return addResult;
        }
        return configUrationShortcut(5,doorList);
    }

    @Override
    public ReturnBody savePermissionByAllDoor(String userNo,List<AcsDeviceDoor> doors){
        List<String> doorList = new ArrayList<>();
        for(AcsDeviceDoor door :doors){
            if("1".equals(door.getType())) {
                doorList.add(door.getDevCode());
            }
        }
        List<String> userList = new ArrayList<>();
        userList.add(userNo);
        ReturnBody addResult = authConfigAdd(userList, doorList);
        if(!"0".equals(addResult.getCode())){
            return addResult;
        }
        return configUrationShortcut(4,doorList);
    }
}
