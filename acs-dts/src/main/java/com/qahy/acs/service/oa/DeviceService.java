package com.qahy.acs.service.oa;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qahy.basic.oa.entity.AcsDeviceDoor;

import java.util.List;

/**
 * @version 1.0
 * @Description //TODO $
 * @Author jujian
 * @Date 2021/11/26 10:12
 **/
public interface DeviceService {
    IPage queryPage(Page<AcsDeviceDoor> page, AcsDeviceDoor entity);

    List<AcsDeviceDoor> queryList(AcsDeviceDoor entity);

    AcsDeviceDoor getInfo(int id);
}
