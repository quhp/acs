package com.qahy.acs.service.task;


import com.qahy.acs.common.bean.ReturnEntity;
import com.qahy.basic.oa.entity.Department;
import com.qahy.basic.oa.entity.User;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0
 * @Description //TODO $
 * @Author jujian
 * @Date 2021/11/11 10:01
 **/
public interface SynTaskService {


    //同步所有组织（初始化）
    ReturnEntity synOrgAll();

    ReturnEntity synOrg(Department dep);

    ReturnEntity synCompany();

    //同步所有人员
    ReturnEntity synUser(User entity);

    //添加人脸
    ReturnEntity synSingle(User entity);

    //通过人员添加权限配置
    ReturnEntity synPermissionByUser(List<String> ids,String devicode);

    //通过组织添加权限配置
    ReturnEntity synPermissionByOrg(List<String>  ids,String devicode);

    //权限配置下发
    ReturnEntity synConf(String indexCode);

    //同步门禁设备
    ReturnEntity synDeviceToAcs(String type);

    //同步门禁设备
    ReturnEntity synDeviceAcsToAcs();

    //同步门禁点
    ReturnEntity synDeviceDoorToAcs();

    //同步门禁数据
    ReturnEntity synEventsDoorToAcs(Map<String, Object> map);

    void synUserInduction();

    void synUserQuit();

    void synUser1(int type);
}
