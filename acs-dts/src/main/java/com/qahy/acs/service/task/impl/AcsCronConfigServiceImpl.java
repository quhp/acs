package com.qahy.acs.service.task.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qahy.acs.entity.task.AcsCronConfig;
import com.qahy.acs.mapper.task.AcsCronConfigMapper;
import com.qahy.acs.service.task.AcsCronConfigService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @version 1.0
 * @Description //TODO
 * @Author jujian
 * @Date 2021/11/22 8:32
 **/
@Service
@DS("db_acs")
public class AcsCronConfigServiceImpl  extends ServiceImpl<AcsCronConfigMapper, AcsCronConfig> implements AcsCronConfigService {

    @Override
    public AcsCronConfig getCronByCode(String code) {
        List<AcsCronConfig> list = baseMapper.selectListByCode(code);
        if(list.size()>0){
            return list.get(0);
        }
        return null;
    }

    @Override
    public List<AcsCronConfig> qureyList(AcsCronConfig config) {
        QueryWrapper<AcsCronConfig> wrapper = new QueryWrapper<AcsCronConfig>();
        return baseMapper.selectList(wrapper);
    }
}
