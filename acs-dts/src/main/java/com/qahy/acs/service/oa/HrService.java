package com.qahy.acs.service.oa;

import com.qahy.acs.common.bean.ReturnBody;

import java.util.Date;

/**
 * @version 1.0
 * @Description //TODO $
 * @Author jujian
 * @Date 2021/12/1 9:16
 **/
public interface HrService {

    ReturnBody userInduction(String userNo);

    ReturnBody userQuit(String userNo);

    ReturnBody userClockin(String userNo, Date startTime, Date endTime);

    ReturnBody userInitialization(int type);
}
