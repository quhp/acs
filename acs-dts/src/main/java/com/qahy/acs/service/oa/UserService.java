package com.qahy.acs.service.oa;

import com.qahy.basic.oa.entity.User;

import java.util.List;

/**
 * @version 1.0
 * @Description //TODO $
 * @Author jujian
 * @Date 2021/11/17 9:41
 **/
public interface UserService {

    List<User> getList(User entity);

    List<User> getByNo(String userNo);

    List<User> getSynList(int type,String userNo);
}
