package com.qahy.acs.service.oa.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qahy.acs.mapper.oa.DeviceMapper;
import com.qahy.acs.service.oa.DeviceService;
import com.qahy.basic.common.util.string.StringUtil;
import com.qahy.basic.oa.entity.AcsDeviceDoor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @version 1.0
 * @Description //TODO
 * @Author jujian
 * @Date 2021/11/26 10:12
 **/
@Service
@DS("db_oa")
public class DeviceServiceImpl implements DeviceService {

    @Autowired
    private DeviceMapper mapper;

    @Override
    public IPage queryPage(Page<AcsDeviceDoor> page, AcsDeviceDoor entity) {
        QueryWrapper<AcsDeviceDoor> wrapper = new QueryWrapper<AcsDeviceDoor>();
        wrapper
                .eq("a.deleted","0")
                .eq("a.status","0")
                .eq(StringUtil.isNotEmpty(entity.getType()),"a.type",entity.getType())
                .eq(StringUtil.isNotEmpty(entity.getDevCode()),"a.dev_code",entity.getDevCode())
                .like(StringUtil.isNotEmpty(entity.getName()),"a.name",entity.getName())
        ;
        page.setRecords(mapper.selectDoorPage(page, wrapper));
        return page;
    }

    @Override
    public List<AcsDeviceDoor> queryList(AcsDeviceDoor entity) {
        QueryWrapper<AcsDeviceDoor> wrapper = new QueryWrapper<AcsDeviceDoor>();
        wrapper
                .eq("a.deleted","0")
                .eq("a.status","0")
                .eq(StringUtil.isNotEmpty(entity.getType()),"a.type",entity.getType())
                .eq(StringUtil.isNotEmpty(entity.getDevCode()),"a.dev_code",entity.getDevCode())
                .eq(StringUtil.isNotEmpty(entity.getPermissionType()),"a.permission_type",entity.getPermissionType())
                .like(StringUtil.isNotEmpty(entity.getName()),"a.name",entity.getName())
        ;
        return mapper.selectDoorList(wrapper);
    }

    @Override
    public AcsDeviceDoor getInfo(int id) {
        return mapper.selectDoorById(id);
    }
}
