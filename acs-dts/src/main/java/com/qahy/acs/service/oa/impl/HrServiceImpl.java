package com.qahy.acs.service.oa.impl;

import com.qahy.acs.common.bean.ReturnBody;
import com.qahy.acs.service.oa.DeviceService;
import com.qahy.acs.service.oa.HrService;
import com.qahy.acs.service.hik.HikApiService;
import com.qahy.acs.service.hik.HikDeviceDoorService;
import com.qahy.acs.service.oa.UserService;
import com.qahy.basic.common.util.date.DateUtil;
import com.qahy.basic.common.util.string.StringUtil;
import com.qahy.basic.oa.entity.AcsDeviceDoor;
import com.qahy.basic.oa.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0
 * @Description //TODO
 * @Author jujian
 * @Date 2021/12/1 9:16
 **/
@Service
public class HrServiceImpl implements HrService {

    @Autowired
    private HikApiService hikApi;

    @Autowired
    private UserService userService;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private HikDeviceDoorService hikDoorService;



    @Override
    public ReturnBody userInduction(String userNo) {
        ReturnBody returnBody = new ReturnBody();
        AcsDeviceDoor doorparams = new AcsDeviceDoor();
        doorparams.setPermissionType("1");//默认权限门禁
        doorparams.setType("1");
        List<AcsDeviceDoor> doorList = deviceService.queryList(doorparams);
        //海康操作
        returnBody = hikApi.addUser(userNo);
        if(!"200".equals(returnBody.getCode())){
            return returnBody;
        }
        returnBody = hikApi.addSingle(userNo);
        if(!"200".equals(returnBody.getCode())){
            return returnBody;
        }
        returnBody = hikApi.savePermission(userNo,doorList);
        if(!"200".equals(returnBody.getCode())){
            return returnBody;
        }
        //中控操作
        doorparams.setType("2");
        doorList = deviceService.queryList(doorparams);
        return new ReturnBody("操作成功");
    }


    @Override
    public ReturnBody userQuit(String userNo) {
        ReturnBody returnBody = new ReturnBody();
        AcsDeviceDoor doorparams = new AcsDeviceDoor();
        doorparams.setType("1");
        List<AcsDeviceDoor> doorList = deviceService.queryList(doorparams);
//        List<HikDeviceDoor> doorlist = hikDoorService.queryList(new HikDeviceDoor());
        //海康操作
        returnBody = hikApi.deleteSingle(userNo);
        if(!"200".equals(returnBody.getCode())){
            return returnBody;
        }
        if(!"200".equals(returnBody.getCode())){
            return returnBody;
        }
        returnBody = hikApi.deletePermissions(doorList);
        if(!"200".equals(returnBody.getCode())){
            return returnBody;
        }
        //中控操作
        doorparams.setType("2");
        doorList = deviceService.queryList(doorparams);
        return new ReturnBody("操作成功");
    }

    @Override
    public ReturnBody userClockin(String userName, Date startTime, Date endTime) {
        String dateStart = "";
        String dateEnd = "";
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("pageNo",1);
        AcsDeviceDoor doorparams = new AcsDeviceDoor();
        doorparams.setType("1");
        doorparams.setDevType("1");
        List<AcsDeviceDoor> doorList = deviceService.queryList(doorparams);
        if(StringUtil.isNotEmpty(userName) && !"0000".equals(userName)){
            map.put("personName",userName);
            //所有
        }
        if(startTime!=null){
            dateStart = DateUtil.dateToHikString(startTime);
        }else {
            dateStart = DateUtil.dateToHikString(DateUtil.getStartDate(DateUtil.yesterday(new Date())));
        }
        if(endTime!=null){
            dateEnd = DateUtil.dateToHikString(endTime);
        }else {
            dateEnd = DateUtil.dateToHikString(DateUtil.getFinallyDate(DateUtil.yesterday(new Date())));
        }
        map.put("pageSize",1000);
        map.put("startTime",dateStart+"+08:00");
        map.put("endTime",dateEnd+"+08:00");
        map.put("receiveStartTime",dateStart+"+08:00");
        map.put("receiveEndTime",dateEnd+"+08:00");

        hikApi.synUserClockin(map);
        return null;
    }

    @Override
    public ReturnBody userInitialization(int type) {
        List<User> list = userService.getSynList(type,"");
        if(type==1){

        }else if(type==2){

        }
        return null;
    }

}
