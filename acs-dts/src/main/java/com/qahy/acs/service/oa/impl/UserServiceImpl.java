package com.qahy.acs.service.oa.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qahy.acs.mapper.oa.UserMapper;
import com.qahy.acs.service.oa.UserService;
import com.qahy.basic.common.util.date.DateUtil;
import com.qahy.basic.common.util.string.StringUtil;
import com.qahy.basic.oa.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @version 1.0
 * @Description //TODO
 * @Author jujian
 * @Date 2021/11/17 9:41
 **/
@Service
@DS("master")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper mapper;


    @Override
    public List<User> getList(User entity) {
        QueryWrapper<User> wrapper = new QueryWrapper<User>();
        wrapper
                .eq("a.state",0)
                .eq("a.is_leave",0)
                .eq("a.status",0)
                .eq("a.leave_type_no","00")

                .eq(StringUtil.isNotEmpty(entity.getCompanyNo()),"a.company_no",entity.getCompanyNo())
                .eq(StringUtil.isNotEmpty(entity.getPartNo()),"a.part_no",entity.getPartNo())
                .eq(StringUtil.isNotEmpty(entity.getUserNo()),"a.user_no",entity.getUserNo())
        ;
        return mapper.selectUserList(wrapper);
    }

    @Override
    public List<User> getByNo(String userNo) {
        QueryWrapper<User> wrapper = new QueryWrapper<User>();
        wrapper
                .eq("a.state",0)
                .eq("a.is_leave",0)
                .eq("a.status",0)
                .eq("a.leave_type_no","00")
                .eq("a.user_no",userNo)
                ;
        return mapper.selectUserList(wrapper);
    }

    @Override
    public List<User> getSynList(int type,String userNo) {

        QueryWrapper<User> wrapper = new QueryWrapper<User>();
        wrapper
                .eq("a.state",0)
                .eq("a.status",0)
                .eq(StringUtil.isNotEmpty(userNo),"a.user_no",userNo)
                .eq(type==1,"a.leave_type_no","00")
                .ne(type==2,"a.leave_type_no","00")
                .gt("a.modify_time", DateUtil.getStartDate(DateUtil.yesterday(new Date())))
                .le("a.modify_time",DateUtil.getFinallyDate(DateUtil.yesterday(new Date())))
        ;
        return mapper.selectUserList(wrapper);
    }
}
