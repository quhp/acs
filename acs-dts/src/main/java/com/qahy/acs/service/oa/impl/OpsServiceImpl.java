package com.qahy.acs.service.oa.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qahy.acs.common.bean.ReturnBody;
import com.qahy.acs.mapper.oa.DeviceMapper;
import com.qahy.acs.service.oa.DeviceService;
import com.qahy.acs.service.oa.OpsService;
import com.qahy.acs.service.hik.HikApiService;
import com.qahy.basic.oa.entity.AcsDeviceDoor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * @version 1.0
 * @Description //TODO
 * @Author jujian
 * @Date 2021/11/26 11:57
 **/
@Service
public class OpsServiceImpl implements OpsService {
    @Autowired
    private DeviceService devService;
    @Autowired
    private HikApiService hikApi;

    @Autowired
    private DeviceMapper devmapper;

    @Override
    public ReturnBody openLimit(String userNo, String ids) {
        QueryWrapper<AcsDeviceDoor> wrapper = new QueryWrapper<AcsDeviceDoor>();
        wrapper
                .eq("a.deleted","0")
                .eq("a.status","0")
                .eq("a.type","1")
                .in("a.id::text " , Arrays.asList(ids.split(",")))
                ;

        List<AcsDeviceDoor> doorlist = devmapper.selectDoorList(wrapper);
        hikApi.savePermission(userNo,doorlist);

        return null;
    }


    @Override
    public ReturnBody outLimit(String userNo, String ids) {
        QueryWrapper<AcsDeviceDoor> wrapper = new QueryWrapper<AcsDeviceDoor>();
        wrapper
                .eq("a.deleted","0")
                .eq("a.status","0")
                .eq("a.type","1")
                .in("a.id::text " , Arrays.asList(ids.split(",")))
        ;
        List<AcsDeviceDoor> doorlist = devmapper.selectDoorList(wrapper);
        return hikApi.outPermission(userNo,doorlist);
    }
}
