package com.qahy.acs.service.task;

import com.qahy.acs.entity.task.AcsCronConfig;

import java.util.List;

/**
 * @version 1.0
 * @Description //TODO $
 * @Author jujian
 * @Date 2021/11/22 8:32
 **/
public interface AcsCronConfigService {

    AcsCronConfig getCronByCode(String code);

    List<AcsCronConfig> qureyList(AcsCronConfig config);
}
