package com.qahy.acs.service.hik.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qahy.acs.hik.entity.HikDeviceAcs;
import com.qahy.acs.mapper.hik.HikDeviceAcsMapper;
import com.qahy.acs.service.hik.HikDeviceAcsService;
import org.springframework.stereotype.Service;

/**
 * @version 1.0
 * @Description //TODO
 * @Author jujian
 * @Date 2021/11/18 14:46
 **/
@Service
@DS("db_acs")
public class HikDeviceAcsServiceImpl extends ServiceImpl<HikDeviceAcsMapper, HikDeviceAcs> implements HikDeviceAcsService {

//    @Autowired
//    private HikDeviceAcsMapper hikDeviceAcsMapper;

    public String test(){
        baseMapper.insert(new HikDeviceAcs());
        return "";
    }
}
