package com.qahy.acs.service.oa.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.qahy.acs.mapper.oa.CompanyMapper;
import com.qahy.acs.service.oa.CompanyService;
import com.qahy.basic.oa.entity.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @version 1.0
 * @Description //TODO
 * @Author jujian
 * @Date 2021/11/26 14:51
 **/
@Service
@DS("db_oa")
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyMapper mapper;
    @Override
    public List<Company> getByCode(String companyNo) {
        return mapper.selectListByCode(companyNo);
    }
}
