package com.qahy.acs.service.hik.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qahy.acs.common.bean.ReturnBody;
import com.qahy.acs.common.bean.ReturnEntity;
import com.qahy.acs.hik.common.base.HikConstants;
import com.qahy.acs.hik.common.bean.HikReturnUtil;
import com.qahy.acs.hik.entity.HikDeviceAcs;
import com.qahy.acs.hik.entity.HikDeviceDoor;
import com.qahy.acs.hik.entity.HikEventsDoor;
import com.qahy.acs.mapper.hik.HikDeviceAcsMapper;
import com.qahy.acs.mapper.hik.HikDeviceDoorMapper;
import com.qahy.acs.mapper.hik.HikEventsDoorMapper;
import com.qahy.acs.service.oa.DepartmentService;
import com.qahy.acs.service.oa.UserService;
import com.qahy.basic.common.util.date.DateUtil;
import com.qahy.basic.common.util.image.ImageUtil;
import com.qahy.basic.common.util.string.StringUtil;
import com.qahy.basic.oa.entity.Company;
import com.qahy.basic.oa.entity.Department;
import com.qahy.acs.hik.common.util.ApiBasicUtil;
import com.qahy.acs.mapper.oa.CompanyMapper;
import com.qahy.acs.mapper.oa.DepartmentMapper;
import com.qahy.acs.service.hik.SynHikTaskService;
import com.qahy.basic.oa.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.*;

/**
 * @version 1.0
 * @Description //TODO
 * @Author jujian
 * @Date 2021/11/11 10:01
 **/
@Service
@DS("master")
public class SynHikTaskServiceImpl implements SynHikTaskService {



    @Autowired
    private ApiBasicUtil hkService;

    @Autowired
    private DepartmentService depService;

    @Autowired
    private UserService userService;
    @Autowired
    private CompanyMapper companyMapper;
    @Autowired
    private DepartmentMapper departmentMapper;
    @Autowired
    private HikDeviceAcsMapper deviceAcsMapper;
    @Autowired
    private HikDeviceDoorMapper deviceDoorMapper;
    @Autowired
    private HikEventsDoorMapper eventsDoorMapper;

    @Override
    public ReturnEntity synOrg(String level) {
        QueryWrapper<Department> wrapper = new QueryWrapper<Department>();
        wrapper
                .eq("a.state",0)
                .eq("a.company_no","CY01")
                ;
        List<Department> list = departmentMapper.selectDepartmentList(wrapper);
        String orgAddsResult = hkService.orgAdds(list);
        return new ReturnEntity(new ReturnBody(orgAddsResult));
    }

    @Override
    public ReturnEntity synOrgAll() {
        List<Company> companyList = companyMapper.selectAll();
        String companysResult = hkService.orgAddsByCompany(companyList);
//        System.out.println("==="+companysResult);
        for(Company company:companyList){
            Department entity = new Department();
            entity.setCompanyNo(company.getCompanyNo());
            entity.setParentNo(company.getCompanyNo());
            List<Department> dep1List = depService.getList(entity);
            String dep1Result = hkService.orgAdds(dep1List);
//            System.out.println(company.getCompanyFName()+"==="+dep1Result);
            for(Department dep1:dep1List){
                entity.setParentNo(dep1.getDepartNo());
                List<Department> dep2List = depService.getList(entity);
                String dep2Result = hkService.orgAdds(dep2List);
//                System.out.println(dep1.getDepartName()+"==="+dep2Result);
                for(Department dep2:dep2List){
                    entity.setParentNo(dep2.getDepartNo());
                    List<Department> dep3List = depService.getList(entity);
                    String dep3Result = hkService.orgAdds(dep3List);
//                    System.out.println(dep2.getDepartName()+"==="+dep3Result);
                    for(Department dep3:dep3List){
                        entity.setParentNo(dep3.getDepartNo());
                        List<Department> dep4List = depService.getList(entity);
                        String dep4Result = hkService.orgAdds(dep4List);
//                        System.out.println(dep3.getDepartName()+"==="+dep4Result);
                        for(Department dep4:dep4List){
                            entity.setParentNo(dep4.getDepartNo());
                            List<Department> dep5List = depService.getList(entity);
                            String dep5Result = hkService.orgAdds(dep5List);
//                            System.out.println(dep4.getDepartName()+"==="+dep5Result);
                            for(Department dep5:dep5List){
                                entity.setParentNo(dep5.getDepartNo());
                                List<Department> dep6List = depService.getList(entity);
                                String dep6Result = hkService.orgAdds(dep6List);
//                                System.out.println(dep5.getDepartName()+"==="+dep6Result);

                            }
                        }
                    }
                }
            }

        }
        return new ReturnEntity(new ReturnBody(companysResult));
    }

    @Override
    public ReturnEntity synCompany() {
        Page<Department> page1 = new Page<Department>();
        page1.setSize(100);
        page1.setCurrent(1);
        List<Company> list = companyMapper.selectAll();
        String companysResult = hkService.orgAddsByCompany(list);
        return new ReturnEntity(new ReturnBody(companysResult));
    }

    @Override
    public ReturnEntity synEmployeeAll(User entity) {
        List<User> list = userService.getList(entity);
        int ps = list.size()/1000 +1;
        for(int i= 0;i<ps;i++){
            int sindex = i*1000;
            int eindex = sindex+999;
            if(list.size()<eindex){
                eindex = list.size();
            }
            String eResult = hkService.userAdds(list.subList(sindex,eindex));
//            System.out.println(sindex+"==="+eResult);
        }

        return new ReturnEntity(new ReturnBody(""));
    }

    @Override
    public ReturnEntity synSingle(User entity) {
        List<User> list = userService.getList(entity);
        String singleResult = "";
        for(User info : list){
//            String singleResult = hkService.singleAdd(info.getEmployeeNo(), ImageUtil.ImageToBase64("指定路径"+info.getEmpid()+"_20.jpg"));
            try {
                singleResult = hkService.singleAdd(info.getUserNo(), ImageUtil.ImageToBase64("E://A04581//cy_acs//images//"+info.getUserNo()+".jpg"));
//                System.out.println(info.getEmployeeNo()+info.getName()+"==="+singleResult);
            }catch (Exception e) {
//                System.out.println("error happens="+info.getEmployeeNo()+info.getName()+"==");
//                System.out.println("error happens="+info.getEmployeeNo()+info.getName()+"=="+e);
            }
//            System.out.println(info.getName()+"==="+singleResult);

        }
        return new ReturnEntity(new ReturnBody(singleResult));
    }

    @Override
    public ReturnEntity synPermissionByEmployee(List<String> ids,String devicode) {
        Map map = new HashMap();
        Map<String,Object> personDatas = new HashMap<>();
        personDatas.put("personDataType","person");
        personDatas.put("indexCodes",ids);
        String str[] = devicode.split(",");
        String resourceIndexCode = "2b2f6b0fe49a4c3ca8b02e0595d3a927";
        List<String> list = Arrays.asList(str);
        if(list.size()>0){
            resourceIndexCode = list.get(0);
        }
        Map<String,Object> resourceInfos = new HashMap<>();
        resourceInfos.put("resourceIndexCode",resourceIndexCode);
        resourceInfos.put("resourceType","door");
//        resourceInfos.put("channelNos",[1]);
//        resourceInfos.put("startTime","");
//        resourceInfos.put("endTime","");

        map.put("personDatas",personDatas);
        map.put("resourceInfos",resourceInfos);
        String permissionResult =  hkService.auth_configAdd(map);
        return new ReturnEntity(new ReturnBody(permissionResult));
    }

    @Override
    public ReturnEntity synPermissionByOrg(List<String> ids,String devicode) {
        Map map = new HashMap();
        Map<String,Object> personDatas = new HashMap<>();
        personDatas.put("personDataType","org");
        personDatas.put("indexCodes",ids);
        String str[] = devicode.split(",");
        String resourceIndexCode = "2b2f6b0fe49a4c3ca8b02e0595d3a927";
        List<String> list = Arrays.asList(str);
        if(list.size()>0){
            resourceIndexCode = list.get(0);
        }
        Map<String,Object> resourceInfos = new HashMap<>();
        resourceInfos.put("resourceIndexCode",resourceIndexCode);
        resourceInfos.put("resourceType","door");
//        resourceInfos.put("channelNos",[1]);
//        resourceInfos.put("startTime","");
//        resourceInfos.put("endTime","");

        map.put("personDatas",personDatas);
        map.put("resourceInfos",resourceInfos);
        String permissionResult =  hkService.auth_configAdd(map);
        return new ReturnEntity(new ReturnBody(permissionResult));
    }

    @Override
    public ReturnEntity synConf(String indexCode) {
        Map map = new HashMap();
//        String str[] = indexCode.split(",");
        Map<String,Object> resourceInfos = new HashMap<>();
        resourceInfos.put("resourceIndexCode",indexCode);
        resourceInfos.put("resourceType","door");
        map.put("resourceInfos",resourceInfos);
        String permissionResult =  hkService.shortcut(4,map);
        return new ReturnEntity(new ReturnBody(permissionResult));
    }

    @Override
    public ReturnEntity synDeviceToAcs(String type) {
        if(StringUtil.isEmpty()){

        }else if("door".equals(type)){
            synDeviceDoorToAcs();
        }else if("acsDevice".equals(type)){
            synDeviceAcsToAcs();
        }else if("reader".equals(type)){

        }
        return null;
    }


    public ReturnEntity synDeviceAcsToAcs() {

        String result = hkService.getList(HikConstants.DEVEICE_ASC_URL,1,1000,"");

        HikReturnUtil rethk = new HikReturnUtil(result);
        if("0".equals(rethk.getCode())){
            Object data = rethk.getData();
//            Map mapdata = (Map)data;
//            if(mapdata.get("total")>0){
                List<Object> list = (List<Object>)((Map)data).get("list");
//                System.out.println("list=="+list);
                for(Object info:list){

                    String mapStr= JSON.toJSONString((Map)info);//-->Map集合转换成JSON字符串
                    HikDeviceAcs entity = JSON.parseObject(mapStr,HikDeviceAcs.class);

                    entity.setId(entity.getIndexCode());
                    entity.setAcsCreateId("sys");
                    entity.setAcsCreateTime(new Date());
                    entity.setAcsUpdateId("sys");
                    entity.setAcsUpdateTime(new Date());
                    entity.setDeleted("0");
                    entity.setOnLine("0");
                    entity.setStatus("0");
                    try {
                        deviceAcsMapper.insert(entity);
                    }catch (Exception e) {

                    }

                }
//            }

        }
        return null;
    }

    public ReturnEntity synDeviceDoorToAcs() {
        String result = hkService.getList(HikConstants.DEVEICE_DOOR_URL,1,1000,"");
        HikReturnUtil rethk = new HikReturnUtil(result);
        if("0".equals(rethk.getCode())){
            Object data = rethk.getData();
            List<Object> list = (List<Object>)((Map)data).get("list");
//            System.out.println("list=="+list);
            for(Object info:list){
                String mapStr= JSON.toJSONString((Map)info);
                HikDeviceDoor entity = JSON.parseObject(mapStr,HikDeviceDoor.class);
                entity.setId(entity.getIndexCode());
                entity.setAcsCreateId("sys");
                entity.setAcsCreateTime(new Date());
                entity.setAcsUpdateId("sys");
                entity.setAcsUpdateTime(new Date());
                entity.setDeleted("0");
                entity.setOnLine("0");
                entity.setStatus("0");
                try {
                    deviceDoorMapper.insert(entity);
                }catch (Exception e) {

                }


            }

        }
        return null;
    }


    @Override
    public ReturnEntity synEventsDoorToAcs(Map<String, Object> map) {
//        Map<String, Object> map = new HashMap<String, Object>();
        int pageNo = (int) map.get("pageNo");
        int pageSize = 1000;
        String dateStart = DateUtil.dateToHikString(DateUtil.getStartDate(DateUtil.yesterday(new Date())));
        String dateEnd = DateUtil.dateToHikString(DateUtil.getFinallyDate(DateUtil.yesterday(new Date())));
        map.put("pageNo",pageNo);
        map.put("pageSize",pageSize);
        map.put("startTime",dateStart);
        map.put("endTime",dateEnd);
        map.put("receiveStartTime",dateStart);
        map.put("receiveEndTime",dateEnd);
        String result = hkService.getListByParams(HikConstants.EVENTS_DOOR,map);
        HikReturnUtil rethk = new HikReturnUtil(result);
        if("0".equals(rethk.getCode())){
            Object data = rethk.getData();
            List<Object> list = (List<Object>)((Map)data).get("list");
            System.out.println("list=="+list);
            for(Object info:list){
                String mapStr= JSON.toJSONString((Map)info);
                HikEventsDoor entity = JSON.parseObject(mapStr, HikEventsDoor.class);
                entity.setId(entity.getEventId());
                entity.setAcsCreateId("sys");
                entity.setAcsCreateTime(new Date());
                entity.setAcsUpdateId("sys");
                entity.setAcsUpdateTime(new Date());
                entity.setDeleted("0");
                entity.setStatus("0");
                try {
                    eventsDoorMapper.insert(entity);
                }catch (Exception e) {

                }

            }
            if((int)((Map)data).get("totalPage") > pageNo ){
                Map<String, Object> mapnew = new HashMap<String, Object>();
                mapnew.put("pageNo",pageNo+1);
                synEventsDoorToAcs(mapnew);
            }

        }
        return null;
    }
}
