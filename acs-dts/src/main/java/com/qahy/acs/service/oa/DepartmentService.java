package com.qahy.acs.service.oa;

import com.qahy.basic.oa.entity.Department;

import java.util.List;

/**
 * @version 1.0
 * @Description //TODO $
 * @Author jujian
 * @Date 2021/11/17 8:31
 **/
public interface DepartmentService {

    List<Department> getList(Department entity);

    List<Department> getByCode(String code);
}
