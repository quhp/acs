package com.qahy.acs.service.oa.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qahy.basic.common.util.string.StringUtil;
import com.qahy.acs.mapper.oa.DepartmentMapper;
import com.qahy.acs.service.oa.DepartmentService;
import com.qahy.basic.oa.entity.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @version 1.0
 * @Description //TODO
 * @Author jujian
 * @Date 2021/11/17 8:32
 **/
@Service
@DS("db_oa")
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    private DepartmentMapper departmentMapper;

    @Override
    public List<Department> getList(Department entity) {
        QueryWrapper<Department> wrapper = new QueryWrapper<Department>();
        wrapper
                .eq("a.state",0)
                .eq("a.is_cancel",0)
                .eq(StringUtil.isNotEmpty(entity.getCompanyNo()),"a.company_no",entity.getCompanyNo())
                .eq(StringUtil.isNotEmpty(entity.getParentNo()),"a.parent_no",entity.getParentNo())
        ;
        return departmentMapper.selectDepartmentList(wrapper);
    }

    @Override
    public List<Department> getByCode(String code) {
        QueryWrapper<Department> wrapper = new QueryWrapper<Department>();
        wrapper
                .eq("a.state",0)
                .eq("a.is_cancel",0)
                .eq(StringUtil.isNotEmpty(code),"a.depart_no",code)
        ;
        return departmentMapper.selectDepartmentList(wrapper);
    }
}
