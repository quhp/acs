package com.qahy.acs.service.dongbao;

import com.qahy.acs.hik.entity.HikEventsDoor;
import com.qahy.basic.dongbao.entity.d1ehr.AttCardData;

/**
 * @version 1.0
 * @Description //TODO $
 * @Author jujian
 * @Date 2021/12/2 14:09
 **/
public interface AttCardDataService {

    AttCardData recordToData(HikEventsDoor record);

    int saveRecord(HikEventsDoor record);
}
