package com.qahy.acs.service.hik;

import com.qahy.acs.hik.entity.HikDeviceDoor;

import java.util.List;

/**
 * @version 1.0
 * @Description //TODO $
 * @Author jujian
 * @Date 2021/11/18 14:51
 **/
public interface HikDeviceDoorService {
    List<HikDeviceDoor> queryList(HikDeviceDoor entity);
}
