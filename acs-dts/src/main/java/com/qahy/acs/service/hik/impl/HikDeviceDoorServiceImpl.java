package com.qahy.acs.service.hik.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qahy.acs.hik.entity.HikDeviceDoor;
import com.qahy.acs.mapper.hik.HikDeviceDoorMapper;
import com.qahy.acs.service.hik.HikDeviceDoorService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @version 1.0
 * @Description //TODO
 * @Author jujian
 * @Date 2021/11/18 14:47
 **/
@Service
@DS("db_acs")
public class HikDeviceDoorServiceImpl extends ServiceImpl<HikDeviceDoorMapper, HikDeviceDoor> implements HikDeviceDoorService {
    @Override
    public List<HikDeviceDoor> queryList(HikDeviceDoor entity) {
        QueryWrapper<HikDeviceDoor> wrapper = new QueryWrapper<HikDeviceDoor>();
        return baseMapper.selectList(wrapper);
    }
}
