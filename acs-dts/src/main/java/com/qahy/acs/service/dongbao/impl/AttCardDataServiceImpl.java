package com.qahy.acs.service.dongbao.impl;

import com.qahy.acs.hik.entity.HikEventsDoor;
import com.qahy.acs.mapper.dongbao.AttCardDataMapper;
import com.qahy.acs.service.oa.UserService;
import com.qahy.acs.service.dongbao.AttCardDataService;
import com.qahy.basic.common.util.date.DateUtil;
import com.qahy.basic.common.util.uuid.UUIDUtil;
import com.qahy.basic.dongbao.entity.d1ehr.AttCardData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @version 1.0
 * @Description //TODO
 * @Author jujian
 * @Date 2021/12/2 14:10
 **/
@Service
public class AttCardDataServiceImpl implements AttCardDataService {

    @Autowired
    private AttCardDataMapper cardDataMapper;

    @Autowired
    private UserService userService;


//    @Override
//    public AttCardData recordToData(HikEventsDoor record) {
//        AttCardData entity = new AttCardData();
//
//        return null;
//    }

    @Override
    public AttCardData recordToData(HikEventsDoor record) {
        AttCardData entity = new AttCardData();
        String etstr = record.getEventTime();
        String[] et = etstr.split("T");
        entity.setID(UUIDUtil.getEhrId(etstr));
        entity.setYYMMDD(et[0]+" 00:00:00.000");
        entity.setCardTime(et[1].substring(0,5));
        entity.setEmpID(userService.getByNo(record.getJobNo()).get(0).getEmpid());
        entity.setEmpNo(record.getJobNo());
        entity.setCardTxt(record.getEventTime()+ UUIDUtil.getNumUUID());
        entity.setIsSigned(0);
        entity.setIsBackup(0);
        entity.setIsDelete(0);
        entity.setModuleType(0);
        entity.setInOrOutFlag(0);
        entity.setIsShowApp(0);
        entity.setCreateTime(DateUtil.toString(new Date(),"yyyy-MM-dd HH:mm:ss"));
        return entity;
    }

    @Override
    public int saveRecord(HikEventsDoor record) {

        if(record.getEventType()==196893){
            try {
                return cardDataMapper.insertCardRecord(recordToData(record));
            }catch (Exception e) {
                System.out.println("ree======="+e);
            }
        }
        return 10001;
    }
}
