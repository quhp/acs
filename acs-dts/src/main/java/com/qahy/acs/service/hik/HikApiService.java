package com.qahy.acs.service.hik;

import com.qahy.acs.common.bean.ReturnBody;
import com.qahy.acs.hik.common.bean.HikReturnUtil;
import com.qahy.basic.oa.entity.AcsDeviceDoor;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0
 * @Description //TODO $
 * @Author jujian
 * @Date 2021/11/26 14:11
 **/
public interface HikApiService {


    //同步所有组织（初始化）
//    ReturnEntity synOrgAll();

    ReturnBody addDepartment(String departmentNo );

    ReturnBody addCompany(String companyNo);

    ReturnBody addUser(String userNo);

    ReturnBody deleteUser(String userNo);

    //添加人脸
    ReturnBody addSingle(String userNo);

    //删除人脸
    ReturnBody deleteSingle(String userNo);

    //添加权限配置
    ReturnBody addPermission(String userNo,String deviceCode);

    //批量添加权限配置
    ReturnBody authConfigAdd(List<String> userNos, List<String> deviceCodes);

    //批量添加权限配置
    ReturnBody authConfigDelete(List<String> userNos, List<String> deviceCodes);

    //下发权限
    ReturnBody confPermission(String deviceCode);

    //批量操作权限
    ReturnBody configUrationShortcut(int taskType,List<String> deviceCodes);

    //删除权限
    ReturnBody deletePermission(String deviceCode);


    //同步门禁设备
    ReturnBody synDeviceToAcs(String type);

    //同步门禁设备
    ReturnBody synDeviceAcsToAcs();

    //同步门禁点
    ReturnBody synDeviceDoorToAcs();


    //门禁数据
    HikReturnUtil eventsDoor(Map<String, Object> map);

    //同步门禁数据
    ReturnBody synEventsDoorToAcs(Map<String, Object> map);

    //同步门禁数据
    ReturnBody synEventsDoorToEhr(Map<String, Object> map);

    //手动同步考勤
    ReturnBody synUserClockin(Map<String, Object> map);



    //删除权限
    ReturnBody deletePermissions(List<AcsDeviceDoor> doors);

    //
    ReturnBody savePermission(String userNo,List<AcsDeviceDoor> doors);

    //
    ReturnBody outPermission(String userNo,List<AcsDeviceDoor> doors);

    ReturnBody savePermissionByAllDoor(String userNo,List<AcsDeviceDoor> doors);
}
