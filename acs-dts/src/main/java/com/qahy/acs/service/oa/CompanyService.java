package com.qahy.acs.service.oa;

import com.qahy.basic.oa.entity.Company;

import java.util.List;

/**
 * @version 1.0
 * @Description //TODO $
 * @Author jujian
 * @Date 2021/11/26 14:51
 **/
public interface CompanyService {
    List<Company> getByCode(String companyNo);
}
