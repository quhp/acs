package com.qahy.acs.mapper.oa;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qahy.basic.oa.entity.Department;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @version 1.0
 * @Description //TODO $
 * @Author jujian
 * @Date 2021/11/10 16:26
 **/
@Repository
@Mapper
@DS("db_oa")
public interface DepartmentMapper extends BaseMapper<Department> {

    List<Department> selectDepartmentList( @Param("ew") QueryWrapper<Department> wrapper);

}

