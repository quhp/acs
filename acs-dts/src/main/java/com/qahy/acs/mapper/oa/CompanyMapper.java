package com.qahy.acs.mapper.oa;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qahy.basic.oa.entity.Company;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author jujian
 * @Description //TODO $todo
 * @Date  2021/11/09 16:29
 * @return
 **/
@Repository
@Mapper
@DS("db_oa")
public interface CompanyMapper extends BaseMapper<Company> {

    List<Company> selectAll();

    List<Company> selectListByCode(String code);

    List<Company> selectCompanyList(@Param("ew") QueryWrapper<Company> wrapper);
}
