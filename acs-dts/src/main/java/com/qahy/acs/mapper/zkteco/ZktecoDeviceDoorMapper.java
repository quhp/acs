package com.qahy.acs.mapper.zkteco;

import com.qahy.acs.zkteco.entity.ZktecoDeviceDoor;

/**
 * @version 1.0
 * @Description //TODO $
 * @Author jujian
 * @Date 2021/11/26 16:52
 **/
public interface ZktecoDeviceDoorMapper {
    ZktecoDeviceDoor selectDoorByCode(String code);
}
