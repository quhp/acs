package com.qahy.acs.mapper.oa;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qahy.basic.oa.entity.AcsDeviceDoor;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @version 1.0
 * @Description //TODO $
 * @Author jujian
 * @Date 2021/11/26 10:29
 **/
@DS("db_oa")
public interface DeviceMapper extends BaseMapper<AcsDeviceDoor> {

    List<AcsDeviceDoor> selectDoorPage(Page<AcsDeviceDoor> page, @Param("ew") QueryWrapper<AcsDeviceDoor> wrapper);

    List<AcsDeviceDoor> selectDoorList(@Param("ew") QueryWrapper<AcsDeviceDoor> wrapper);

    AcsDeviceDoor selectDoorById(int id);
}
