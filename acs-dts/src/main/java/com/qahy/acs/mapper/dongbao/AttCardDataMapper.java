package com.qahy.acs.mapper.dongbao;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qahy.basic.dongbao.entity.d1ehr.AttCardData;

/**
 * @version 1.0
 * @Description //TODO $
 * @Author jujian
 * @Date 2021/11/30 15:17
 **/
@DS("db_ehr")
public interface AttCardDataMapper extends BaseMapper<AttCardData> {
    int insertCardRecord(AttCardData recordToData);
}
