package com.qahy.acs.mapper.oa;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qahy.basic.oa.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @version 1.0
 * @Description //TODO $
 * @Author jujian
 * @Date 2021/11/17 10:16
 **/
@DS("db_oa")
public interface UserMapper extends BaseMapper<User> {

    List<User> selectUserList(@Param("ew") QueryWrapper<User> wrapper);
}
