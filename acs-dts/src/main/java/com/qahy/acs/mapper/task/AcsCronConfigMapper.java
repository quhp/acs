package com.qahy.acs.mapper.task;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qahy.acs.entity.task.AcsCronConfig;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @version 1.0
 * @Description //TODO $
 * @Author jujian
 * @Date 2021/11/22 8:31
 **/
@Mapper
@DS("db_acs")
public interface AcsCronConfigMapper  extends BaseMapper<AcsCronConfig> {

    @Select("select * from acs_cron_config where code = #{code}")
    List<AcsCronConfig> selectListByCode(String code);
}
