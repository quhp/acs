package com.qahy.acs.mapper.hik;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qahy.acs.hik.entity.HikDeviceAcs;

/**
 * @version 1.0
 * @Description //TODO $
 * @Author jujian
 * @Date 2021/11/18 14:44
 **/
@DS("db_acs")
public interface HikDeviceAcsMapper extends BaseMapper<HikDeviceAcs> {
}
