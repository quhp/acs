package com.qahy.acs.task;

import com.qahy.acs.entity.task.AcsCronConfig;
import com.qahy.acs.service.hik.SynHikTaskService;
import com.qahy.acs.service.task.AcsCronConfigService;
import com.qahy.acs.service.task.SynTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.support.CronTrigger;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0
 * @Description //TODO 任务日程配置
 * @Author jujian
 * @Date 2021/11/22 8:52
 **/
//@Configuration
//@EnableScheduling
public class ScheduleConfig implements SchedulingConfigurer {

    @Autowired
    private AcsCronConfigService cronService;

    @Autowired
    private SynTaskService taskService;

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        List<AcsCronConfig> crons = cronService.qureyList(new AcsCronConfig());
        for(AcsCronConfig cron: crons){
            taskRegistrar.addTriggerTask(
                    () -> System.out.println("执行定时任务: " + LocalDateTime.now().toLocalTime()),triggerContext -> {
                        if (cron==null) {
                            return new CronTrigger("").nextExecutionTime(triggerContext);
                        }else if("DEVEICE_ASC".equals(cron.getCode())){
                            taskService.synDeviceAcsToAcs();
                        }else if("DEVEICE_DOOR".equals(cron.getCode())){
                            taskService.synDeviceDoorToAcs();
                        }else if("EVENTS_DOOR".equals(cron.getCode())){
                            Map map = new HashMap<String, Object>();
                            map.put("pageNo",1);
                            taskService.synEventsDoorToAcs(map);
                        }else if("USER_INDUCTION".equals(cron.getCode())){
                            //
                            taskService.synUserInduction();
                        }else if("USER_QUIT".equals(cron.getCode())){
                            //
                            taskService.synUserQuit();
                        }else {
                            return new CronTrigger(cron.getCron()).nextExecutionTime(triggerContext);
                        }
                        //2.3 返回执行周期(Date)
                        return new CronTrigger(cron.getCron()).nextExecutionTime(triggerContext);
                    }
            );
        }
    }
}
