package com.qahy.acs.entity.task;

import lombok.Data;

/**
 * @version 1.0
 * @Description //TODO
 * @Author jujian
 * @Date 2021/11/22 8:29
 **/
@Data
public class AcsCronConfig {

    private Integer id;

    private String code;

    private String cron;

    private String name;

    private String apiurl;

    private String type;

    private String status;
}
