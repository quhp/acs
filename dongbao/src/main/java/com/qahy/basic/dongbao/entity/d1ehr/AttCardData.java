package com.qahy.basic.dongbao.entity.d1ehr;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @version 1.0
 * @Description //TODO
 * @Author jujian
 * @Date 2021/11/25 8:39
 **/
@Data
public class AttCardData {

    private Long ID;

    private String YYMMDD;

    private String CardTime;
    private String EmpID;
    private String EmpNo;
    private int IntCardTime;
    private int CCID;
    private String CardTxt;
    private int IsSigned;
    private String SignTypeNo;
    private int IsBackup;
    private int IsDelete;
    private String CheckCode;
    private int IsError;
    private String PID;
    private String CardID;
    private String InsTime;//datetime
    private String GetTime;
    private int ModuleType;
    private int InOrOutFlag;
    private int CardFlag;
    private int SaveInOrOutFlag;
    private int ValidType;
    private String EmpPhoto;
    private String TakePhoto;
    private String lat;//money
    private String lng;//money
    private String signplace;
    private int IsShowApp;
    private String CardDescription;
    private String CardBackup;
    private String CreateDate;//datetime
    private String CreateUser;
    private String ModifiedDate;//datetime
    private String ModifiedUser;
    private String Remark;
    private int AttachCount;
    private int DataSource;
    private Long ARecordID;
    private String CreateUserName;
    private String ModifiedUserName;
    private String CreateTime;
    private String ModifiedTime;
    private String VailidResult;
    private String VailidFailMsg;
    private String Temperature;
    private String imeId;
    private String AttCardDeviceId;

}
