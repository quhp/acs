/*
 * Project Name: zksecurity-adms
 * File Name: JNIForAdms.java
 * Copyright: Copyright(C) 1985-2014 ZKTeco Inc. All rights reserved.
 */
package zk.jni;

/**
 * !!!!!!!!!!!!!!!!!
 * 请注意 这个类务必放在  zk.jni包下，不可放在自定义包下!!!!!!!!
 * 
 * @author <a href="mailto:gordon.zhang@zkteco.com">张法军</a>
 * @version 6.0
 * @since Nov 3, 2014 4:25:35 PM
 */
public class JavaToAdmsPullSDK
{
	/**
	 * 连接设备，连接成功后返回连接句柄
	 * 
	 * @author <a href="mailto:gordon.zhang@zkteco.com">张法军</a>
	 * @since Nov 3, 2014 4:42:12 PM
	 * @param connectParam
	 * @return
	 */
	public static native long Connect(String connectParam);
	
	
	/**
	 * 连接设备，连接成功后返回连接句柄
	 * @author kaichun-li
	 * @since 2015年10月15日 下午5:02:25
	 * @param connectParam
	 * @param callbackResult
	 * @return
	 */
	
	public static native long ConnectExt(String connectParam, int[] callbackResult);

	/**
	 * 断开与设备的连接
	 * 
	 * @author <a href="mailto:gordon.zhang@zkteco.com">张法军</a>
	 * @since Nov 3, 2014 4:43:49 PM
	 * @param hcommpro
	 * @return
	 */
	public static native int Disconnect(long hcommpro);

	/**
	 * 设置控制器参数，例如设备号、门磁类型、锁驱动时间、读卡间隔等
	 * 
	 * @author <a href="mailto:gordon.zhang@zkteco.com">张法军</a>
	 * @since Nov 3, 2014 4:44:31 PM
	 * @param hcommpro
	 * @param items
	 * @return
	 */
	public static native int SetDeviceParam(long hcommpro, String items);

	/**
	 * 读取控制器参数，例如设备号、门磁类型、锁驱动时间、读卡间隔等
	 * 
	 * @author <a href="mailto:gordon.zhang@zkteco.com">张法军</a>
	 * @since Nov 3, 2014 5:49:24 PM
	 * @param hcommpro
	 * @param callbackResult
	 * @param item
	 * @return
	 */
	public static native int GetDeviceParam(long hcommpro, byte[] callbackResult, String item);

	/**
	 * 控制控制器动作
	 * 
	 * @author <a href="mailto:gordon.zhang@zkteco.com">张法军</a>
	 * @since Nov 3, 2014 6:03:27 PM
	 * @param hcommpro
	 * @param operationID
	 * @param param1
	 * @param param2
	 * @param param3
	 * @param param4
	 * @param options
	 * @return
	 */
	public static native int ControlDevice(long hcommpro, int operationID, int param1, int param2, int param3, int param4, String options);

	/**
	 * 设置数据到设备
	 * 
	 * @author <a href="mailto:gordon.zhang@zkteco.com">张法军</a>
	 * @since Nov 4, 2014 8:58:07 AM
	 * @param hcommpro
	 * @param tableName
	 * @param data
	 * @param options
	 * @return
	 */
	public static native int SetDeviceData(long hcommpro, String tableName, String data, String options);

	/**
	 * 从设备读取数据
	 * 
	 * @author <a href="mailto:gordon.zhang@zkteco.com">张法军</a>
	 * @since Nov 4, 2014 9:01:30 AM
	 * @param hcommpro
	 * @param callbackResult
	 * @param tableName
	 * @param fieldNames
	 * @param filter
	 * @param options
	 * @return
	 */
	public static native int GetDeviceData(long hcommpro, byte[] callbackResult, String tableName, String fieldNames, String filter, String options);

	/**
	 * 读取设备中的记录总数信息，返回指定数据的记录条数
	 * 
	 * @author <a href="mailto:gordon.zhang@zkteco.com">张法军</a>
	 * @since Nov 4, 2014 9:06:20 AM
	 * @param hcommpro
	 * @param tableName
	 * @param filter
	 * @param options
	 * @return
	 */
	public static native int GetDeviceDataCount(long hcommpro, String tableName, String filter, String options);

	/**
	 * 删除设备中的数据，例如用户信息、时间段等数据
	 * 
	 * @author <a href="mailto:gordon.zhang@zkteco.com">张法军</a>
	 * @since Nov 4, 2014 9:07:32 AM
	 * @param hcommpro
	 * @param tableName
	 * @param data
	 * @param options
	 * @return
	 */
	public static native int DeleteDeviceData(long hcommpro, String tableName, String data, String options);

	/**
	 * 获取设备产生的实时事件记录以及设备的门状态、报警状态等
	 * 
	 * @author <a href="mailto:gordon.zhang@zkteco.com">张法军</a>
	 * @since Nov 4, 2014 9:10:04 AM
	 * @param hcommpro
	 * @param callbackResult
	 * @return
	 */
	public static native int GetRTLog(long hcommpro, byte[] callbackResult);
	
	/**
	 * @author kaichun-li
	 * @since 2015年10月20日 上午10:45:52
	 * @param hcommpro
	 * @param callbackResult
	 * @return
	 */
	public static native int GetRTLogExt(long hcommpro, byte[] callbackResult);
	
	/**
	 * 搜索局域网内的门禁控制器
	 * 
	 * @author <a href="mailto:gordon.zhang@zkteco.com">张法军</a>
	 * @since Nov 4, 2014 9:12:51 AM
	 * @param commType
	 * @param address
	 * @param callbackResult
	 * @return
	 */
	public static native int SearchDevice(String commType, String address, byte[] callbackResult);

	/**
	 * UDP广播方式修改控制器IP地址
	 * 
	 * @author <a href="mailto:gordon.zhang@zkteco.com">张法军</a>
	 * @since Nov 4, 2014 9:13:55 AM
	 * @param commType
	 * @param address
	 * @param buffer
	 * @return
	 */
	public static native int ModifyIPAddress(String commType, String address, String buffer);

	/**
	 * 获取返回值错误码
	 * 
	 * @author <a href="mailto:gordon.zhang@zkteco.com">张法军</a>
	 * @since Nov 4, 2014 9:16:04 AM
	 * @return
	 */
	public static native int PullLastError();

	/**
	 * 将文件从PC传送到设备
	 * 
	 * @author <a href="mailto:gordon.zhang@zkteco.com">张法军</a>
	 * @since Nov 4, 2014 9:17:24 AM
	 * @param hcommpro
	 * @param fileName
	 * @param buffer
	 * @param bufferSize
	 * @param options
	 * @return
	 */
	public static native int SetDeviceFileData(long hcommpro, String fileName, String buffer, int bufferSize, String options);

	/**
	 * 从设备获取文件到PC
	 * 
	 * @author <a href="mailto:gordon.zhang@zkteco.com">张法军</a>
	 * @since Nov 4, 2014 9:19:03 AM
	 * @param hcommpro
	 * @param callbackResult
	 * @param fileName
	 * @param options
	 * @return
	 */
	public static native int GetDeviceFileData(long hcommpro, byte[] callbackResult, String fileName, String options);

	/**
	 * 用来处理设备备份的文件，如SD卡中的备份文件等
	 * 
	 * @author <a href="mailto:gordon.zhang@zkteco.com">张法军</a>
	 * @since Nov 4, 2014 9:20:42 AM
	 * @param revBuf
	 * @param fileLen
	 * @param callbackResult
	 * @param outSize
	 * @return
	 */
	public static native int ProcessBackupData(byte[] revBuf, int fileLen, byte[] callbackResult, int outSize);
	
	/**
	 * 广播获取参数
	 * 
	 * @author <a href=mailto:yulong.dai@zkteco.com>yulong.dai</a>
	 * @since 2017年1月5日 上午10:45:53
	 * @param commType
	 * @param address
	 * @param paramItems
	 * @param callbackResult
	 * @return
	 */
	public static native int SearchDeviceEx(String commType, String address, String paramItems, byte[] callbackResult);
	
}
