package com.qahy.acs.zkteco.demo;


import com.qahy.acs.zkteco.common.bean.SdkResult;
import com.qahy.acs.zkteco.service.impl.AdmsSdkServiceImpl;

import javax.annotation.Resource;

public class App {
    @Resource
//    private AdmsSdkService admsSdkService;
    public static void main(String[] args) {
        AdmsSdkServiceImpl admsSdkService = new AdmsSdkServiceImpl();
       //1 在局域网内发送udp广播，进行设备的搜索
//        SdkResult sdkResult = admsSdkService.searchDevice("Ethernet","172.99.20.201");
//        SdkResult sdkResult = admsSdkService.searchDevice("UDP","255.255.255.255");
//        System.out.println(sdkResult.toString());

        //2 C3不支持此函数。
        /*sdkResult = admsSdkService.searchDeviceEx("~SerialNumber,MaxMCUCardBits,MachineType,AccSupportFunList");
        System.out.println(sdkResult.toString());*/
        //有可能会搜索到多台设备，下面的Ip地址是选择搜索到的第一台设备的Ip
//        String ip = Arrays.asList(sdkResult.getData().split(",")).stream().filter((str)->str.startsWith("IP")).collect(Collectors.toList()).get(0).split("=")[1];
//        System.out.println("设备的ip地址是:\t"+ip);

        //2.建立设备连接
        SdkResult sdkResult = admsSdkService.connectExt("protocol=TCP,ipaddress=10.10.3.201,port=4370,timeout=4000,passwd=",new int[4]);
        //3.设置设备时间
        System.out.println(sdkResult.toString());
        int hCommPro = sdkResult.getResult();//获取句柄，后续根据此句柄，进行设备的操作、查询等
        System.out.println("获取到的句柄值为..."+hCommPro);
//        sdkResult = admsSdkService.setDeviceParam(hCommPro,"DateTime="+DateUtils.convertDate2Str(new Date()));//时间转换成时间戳算法请参照DateUtils

        sdkResult = admsSdkService.setDeviceParam(hCommPro,"Door1KeepOpenTimeZone=0");
        System.out.println(sdkResult.toString());

        //4.获取设备参数，根据传入的item，动态获取想得到的信息
//        String item = AdmsSdkService.ACP_OPTIONS;
//        sdkResult = admsSdkService.getDeviceParam(hCommPro,new byte[1024*1024],item);
//        System.out.println(sdkResult.toString());



        //5.控制设备，如远程开门等
        sdkResult = admsSdkService.controlDevice(hCommPro,1,1,1,2,5,"");//远程开门
        System.out.println(sdkResult.toString());


        String data = "TimezoneId=6\tWedTime1=127600541\t";
        System.out.println("----------------------设置设备数据----------------------");
        sdkResult = admsSdkService.setDeviceData(hCommPro,"timezone",data,"");
        System.out.println(sdkResult.toString());

        //6.设置设备数据，以用户表举例
        data = "CardNo=1023\tPin=3\tPassword=123456\tGroup=0\tStartTime=0\tEndTime=0\tSuperAuthorize=0\t";
        System.out.println("----------------------设置设备数据----------------------");
        sdkResult = admsSdkService.setDeviceData(hCommPro,"user",data,"");
        System.out.println(sdkResult.toString());


        sdkResult = admsSdkService.deleteDeviceData(hCommPro, "userauthorize", "*", "");
        System.out.println("删除门禁权限组结果"+sdkResult.toString());

        data = "Pin=3\tAuthorizeTimezoneId=6\tAuthorizeDoorId=1\t";
        System.out.println("----------------------设置设备数据----------------------");
        sdkResult = admsSdkService.setDeviceData(hCommPro,"userauthorize",data,"");
        System.out.println(sdkResult.toString());

        //7.获取设备数据,根据table不同，获取设备内部不同类型的数据，以用户表举例
        byte[] callback = new byte[10240*10240];
        //206设备启动  27人未登记
        //table的取值 transaction,user,templatev10,extuser,fvtemplate,facev7,mulcarduser,
        //time_second字段转换成时间 算法请参照DateUtils工具类
        System.out.println("----------------------获取用户设备数据----------------------");
        sdkResult = admsSdkService.getDeviceData(hCommPro,callback,"user","*","","NewRecord");
        System.out.println(sdkResult.toString());
        System.out.println("----------------------获取记录设备数据----------------------");
        sdkResult = admsSdkService.getDeviceData(hCommPro,callback,"transaction","*","","");
        System.out.println(sdkResult.toString());
        System.out.println("----------------------删除权限组数据----------------------");
        sdkResult = admsSdkService.deleteDeviceData(hCommPro, "multimcard", "*", "");
        System.out.println(sdkResult.toString());
        sdkResult = admsSdkService.deleteDeviceData(hCommPro, "transaction", "*", "");
        System.out.println(sdkResult.toString());
        System.out.println("----------------------获取多卡开门设备数据----------------------");
        sdkResult = admsSdkService.getDeviceData(hCommPro,callback,"transaction","*","","");
        System.out.println(sdkResult.toString());

       /* //8.获取实时记录*/
        System.out.println("----------------------获取实时记录----------------------");
        sdkResult = admsSdkService.getRTLog(hCommPro,new byte[1024*1024] );
        System.out.println(sdkResult.toString());


    }
}
