package com.qahy.acs.zkteco.common.util;

import com.qahy.acs.zkteco.common.base.ZKTecoConstants;
import com.qahy.acs.zkteco.common.bean.SdkResult;
import com.qahy.acs.zkteco.common.bean.ZkParams;
import com.qahy.acs.zkteco.common.util.param.ParamUtil;
import com.qahy.acs.zkteco.entity.ZktecoDeviceDoor;
import com.qahy.acs.zkteco.service.AdmsSdkService;
import org.springframework.beans.factory.annotation.Autowired;
import zk.jni.JavaToAdmsPullSDK;
//import com.qahy.basic.common.util.string.StringUtil;


/**
 * @version 1.0
 * @Description //TODO
 * @Author jujian
 * @Date 2021/11/23 8:46
 **/
//@Service
public class SdkUtil {

    @Autowired
    private AdmsSdkService admsSdkService;

    public int getConnectExt(ZktecoDeviceDoor door) {
        String  connectParam = ParamUtil.connectParam(door.getIp());
        SdkResult sdkResult = admsSdkService.connectExt(connectParam,new int[4]);
        return sdkResult.getResult();
    }

    public SdkResult setUserData(long hCommPro, String data, String options) {
        SdkResult sdkResult = new SdkResult();
        try {
            int result = JavaToAdmsPullSDK.SetDeviceData(hCommPro, ZKTecoConstants.TB_USER, data, options);
            sdkResult.setResult(result);
            sdkResult.setData("");
        } catch (Error e) {
//            sdkResult.setResult(LOADLIBRARY_FAILURE);
        }
        return sdkResult;
    }


    public SdkResult setUserauthorizeData(long hCommPro,  String data, String options) {
        SdkResult sdkResult = new SdkResult();
        try {
            int result = JavaToAdmsPullSDK.SetDeviceData(hCommPro, ZKTecoConstants.TB_USERAUTHORIZE, data, options);
            sdkResult.setResult(result);
            sdkResult.setData("");
        } catch (Error e) {
//            sdkResult.setResult(LOADLIBRARY_FAILURE);
        }
        return sdkResult;
    }

}
