package com.qahy.acs.zkteco.common.util.param;

import com.qahy.acs.zkteco.common.bean.SdkResult;
import com.qahy.acs.zkteco.common.bean.ZkParams;
import com.qahy.acs.zkteco.service.AdmsSdkService;
import com.qahy.basic.common.util.string.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * @version 1.0
 * @Description //TODO
 * @Author jujian
 * @Date 2021/11/23 8:46
 **/
//@Service
public class ParamUtil {

    @Autowired
    private AdmsSdkService admsSdkService;


    //生成参数
    public static String connectParam(String ip){
        return "protocol=TCP,ipaddress="+ip+",port=4370,timeout=4000,passwd=";
    }

    //生成参数
    public static String connectParam(String ip,String port){
        return "protocol=TCP,ipaddress="+ip+",port="+port+",timeout=4000,passwd=";
    }

    //生成参数
    public static String connectParam(String ip,int timeout){
        return "protocol=TCP,ipaddress="+ip+",port=4370,timeout="+timeout+",passwd=";
    }

    //生成参数
    public static String userParam(int CardNo,int Pin,String Password,String userName){
        return "CardNo="+CardNo+"\tPin="+Pin+"\tPassword="+Password+"\tGroup=0\tStartTime=0\tEndTime=0\tName="+userName+"\tSuperAuthorize=0\t";
    }

    //生成参数
    public static String connectParam(ZkParams params){
        if(StringUtil.isEmpty(params.getProtocol())){
            params.setProtocol("TCP");
        }
        if(StringUtil.isEmpty(params.getIp())){

        }
        if(StringUtil.isEmpty(params.getPort())){
            params.setPort("4370");
        }
        if(params.getTimeout()==null){
            params.setTimeout(400);
        }
        if(StringUtil.isEmpty(params.getPasswd())){
            params.setPasswd("");
        }
        return "protocol="+params.getProtocol()+",ipaddress="+params.getIp()+",port="+params.getPort()+",timeout="+params.getTimeout()+",passwd="+params.getPasswd();
    }


    public SdkResult Connect(ZkParams params){
        if(StringUtil.isEmpty(params.getProtocol())){
            params.setProtocol("TCP");
        }
        if(StringUtil.isEmpty(params.getIp())){

        }
        if(StringUtil.isEmpty(params.getPort())){
            params.setPort("4370");
        }
        if(params.getTimeout()==null){
            params.setTimeout(400);
        }
        if(StringUtil.isEmpty(params.getPasswd())){
            params.setPasswd("");
        }
        String connectParam = "protocol="+params.getProtocol()+",ipaddress="+params.getIp()+",port="+params.getPort()+",timeout="+params.getTimeout()+",passwd="+params.getPasswd();

        return admsSdkService.connect(connectParam);
    }

}
