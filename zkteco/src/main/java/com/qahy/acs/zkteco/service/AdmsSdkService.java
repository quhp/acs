package com.qahy.acs.zkteco.service;

import com.qahy.acs.zkteco.common.bean.SdkResult;

/**
 * @version 1.0
 * @Description //TODO $
 * @Author jujian
 * @Date 2021/11/15 13:55
 **/
public interface AdmsSdkService {

    SdkResult searchDevice(String commType,String ipaddress);

    SdkResult connect(String connectParam);

    SdkResult connectExt(String connectParam, int[] callbackResult);

    SdkResult setDeviceData(long hCommPro, String tableName, String data, String options);

    SdkResult deleteDeviceData(long hCommPro, String tableName, String data, String options);
}
