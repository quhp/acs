package com.qahy.acs.zkteco.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * @version 1.0
 * @Description //TODO
 * @Author jujian
 * @Date 2021/11/26 16:30
 **/
@Data
public class ZktecoDeviceDoor {

    private String id;
    private String name;
    private String controllerCode;
    private String doorNo;
    private String effectiveTime;
    private String openTime;
    private String lockTimes;
    private String gateMagnetismDelayed;
    private String verificationType;
    private String gateMagnetismType;
    private String outIn;
    private String acsCreateId;
    private String acsCreateTime;
    private String acsUpdateId;
    private String acsUpdateTime;
    private String deleted;

    @TableField(exist = false)
    private String ip;
    @TableField(exist = false)
    private String port;

}
