package com.qahy.acs.zkteco.common.bean;

import lombok.Data;

/**
 * @version 1.0
 * @Description //TODO 中控参数公共类
 * @Author jujian
 * @Date 2021/11/29 15:14
 **/
@Data
public class ZkParams {

    private String protocol;
    private String ip;
    private String port;
    private Integer timeout;
    private String passwd;

}
